from scrapy.spiders import Spider
from scrapy.http import Request
import itertools
from .helpers import format_url
from .items import AmazonItemLoader


class AmazonPage:

    def __init__(self, response):

        self.response = response
        self.style_1 = response.css(".s-ref-indent-two > div:nth-child(1) > li")
        self.style_2 = response.css("p.seeMore")
        self.style_3 = response.css(".left_nav > ul:nth-child(9) > li")
        self.style_4 = response.css("ul.a-unordered-list:nth-child(2)>div>li")
        self.style_5 = response.css("div.left_nav:nth-child(4) > ul> li")
        self.style_6 = response.css("div.acs-ln-links>ul>li")
        self.items_selector = response.css(".s-result-list>li")
        self.styles = [self.style_1, self.style_2, self.style_3, self.style_4, self.style_5, self.style_6]

    def get_categories(self):

        url_list = []
        names_list = []

        for link in itertools.chain(*self.styles):
            # iterate over results from all selector styles
            name = link.css("a>span::text").extract_first()
            url = link.css("a::attr(href)").extract_first()

            if url not in url_list \
                    and name is not None:

                names_list.append(name)
                url_list.append(url)

            elif name is None:

                name = link.css("a::text").extract_first()

                if url not in url_list:

                    names_list.append(name)
                    url_list.append(url)

        if len(url_list) != 0:

            return url_list

        else:

            for link in self.response.css("div.acswidget>div>div>div"):

                name = link.css("div>div>a>img::attr(alt)").extract_first()
                url = link.css("div>div>a::attr(href)").extract_first()

                if url not in url_list \
                        and name is not None:

                    names_list.append(name)
                    url_list.append(url)

            return url_list

    def generate_request(self, url, callback):

        if url is not None:

            url = self.response.urljoin(url)
            # url = format_url(url)
            request = Request(url=url, callback=callback)

            return request

    def get_item_requests(self, callback):

        requests = []

        for item in self.items_selector:
            # Generate requests for item pages

            url = item.css("div>div>div>a::attr(href)").extract_first()
            url = self.response.urljoin(url)

            request = Request(url, callback=callback)

            cat_0 = self.response.css("li.s-ref-indent-neg-micro:nth-child(1) > span:nth-child(1) > a:nth-child(1) > span:nth-child(1)::text").extract_first()
            cat_1 = self.response.css("li.s-ref-indent-neg-micro:nth-child(2) > span:nth-child(1) > a:nth-child(1) > span:nth-child(1)::text").extract_first()
            cat_2 = self.response.css("li.s-ref-indent-neg-micro:nth-child(3) > span:nth-child(1) > a:nth-child(1) > span:nth-child(1)::text").extract_first()
            cat_3 = self.response.css("li.s-ref-indent-neg-micro:nth-child(4) > span:nth-child(1) > a:nth-child(1) > span:nth-child(1)::text").extract_first()
            cat_4 = self.response.css("li.s-ref-indent-neg-micro:nth-child(5) > span:nth-child(1) > a:nth-child(1) > span:nth-child(1)::text").extract_first()
            cat_5 = self.response.css("li.s-ref-indent-neg-micro:nth-child(6) > span:nth-child(1) > a:nth-child(1) > span:nth-child(1)::text").extract_first()
            cat_6 = self.response.css("li.s-ref-indent-neg-micro:nth-child(7) > span:nth-child(1) > a:nth-child(1) > span:nth-child(1)::text").extract_first()
            cat_end = self.response.css("h4.a-size-small:nth-child(1)::text").extract_first()
            request.meta['cat_0'] = cat_0
            request.meta['cat_1'] = cat_1
            request.meta['cat_2'] = cat_2
            request.meta['cat_3'] = cat_3
            request.meta['cat_4'] = cat_4
            request.meta['cat_5'] = cat_5
            request.meta['cat_6'] = cat_6
            request.meta['cat_end'] = cat_end

            requests.append(request)

        return requests

    def paginate(self, callback):

        paginate = self.response.css("#pagnNextLink::attr(href)").extract_first()
        paginate = self.response.urljoin(paginate)
        # paginate = format_url(paginate)
        if paginate:
            # paginate through the category to get all items
            request = Request(url=paginate, callback=callback)
            return request

    def get_ids(self, selector):

        style = self.response.css(str(selector))

        for a in style:

            id = a.css("::attr(id)").extract_first()

            if id is not None \
                    and "-1" not in id:

                yield id

    def find_styles(self, selector_list):

        style_1 = self.response.css("#twister")
        style_2 = self.response.css("#twister_feature_div")
        style_3 = self.response.css("#twisterContainer")
        style_4 = self.response.css("#twister>div")
        style_5 = self.response.css("#twister_feature_div>div")
        style_6 = self.response.css("#twisterContainer>div")
        style_7 = self.response.css("#vas-ppd-fake-twister")
        style_8 = self.response.css("#vas-ppd-fake-twister>div")
        style_9 = self.response.css("#twister>ul>li")
        style_10 = self.response.css("#twister_feature_div>ul>li")
        style_11 = self.response.css("#twisterContainer>ul>li")
        style_12 = self.response.css("#vas-ppd-fake-twister>ul>li")
        style_13 = self.response.css("#vasTwisterRow>ul>li")
        style_14 = self.response.css("#vasTwisterRow")

        feature_styles = [style_1, style_2, style_3, style_4, style_5, style_6, style_7, style_8, style_9, style_10,
                          style_11, style_12, style_13, style_14]
        new_selectors = []

        for x in itertools.chain(*feature_styles):

            selector = x.css("::attr(id)").extract_first()

            if selector not in selector_list \
                    and selector is not None:
                new_selectors.append(selector)

        return new_selectors

    def get_item(self, deal_name, option, text, best_sellers_rank, asin, star_rating, star_rating_5,
                 star_rating_4, star_rating_3, star_rating_2, star_rating_1, prime_eligible, date_available,
                 model_number):

        deal_name = deal_name
        option = option
        text = text
        bullets = []
        info = []
        info_type = []

        """store asin and price in variables"""
        # asin = self.response.css("div#cerberus-data-metrics::attr(data-asin)").extract_first()

        """pull information from details section and append key/value pairs to two lists"""
        for link in self.response.css("div.column>div>div>div>div>table>tbody>tr"):
            info.append(link.css("td.value::text").extract_first())
            info_type.append(link.css("td.label::text").extract_first())

        """pull bullet points from page and append to list"""
        for link in self.response.css("#feature-bullets>ul>li"):
            bullets.append(link.css("span::text").extract_first())

        """create info dict from key value pairs in lists info and info type"""
        info_dict = dict(zip(info_type, info))

        """create a list of field names and instantiate dynamic item class creation and item loader"""
        # fields = [x for x in info_type]
        # DynamicItem = create_item_class("Item", fields)
        # dynamicitem = DynamicItem()
        # loader = ItemLoader(item=dynamicitem, response=response)

        # def clean_text(value):
        #     '''output processor for cleaning text based fields'''
        #     if "\\n" not in value:
        #         return value.strip()

        loader = AmazonItemLoader(selector=self.response)

        loader.add_value("deal_name", deal_name)
        loader.add_value("price", text)
        loader.add_value("option", option)
        loader.add_value("best_sellers_rank", best_sellers_rank)
        loader.add_value("asin", asin)
        loader.add_value("star_rating", star_rating)
        loader.add_value("star_rating_5", star_rating_5)
        loader.add_value("star_rating_4", star_rating_4)
        loader.add_value("star_rating_3", star_rating_3)
        loader.add_value("star_rating_2", star_rating_2)
        loader.add_value("star_rating_1", star_rating_1)
        loader.add_value("prime_eligible", prime_eligible)
        loader.add_value("date_available", date_available)
        loader.add_value("model_number", model_number)
        loader.add_value("item_url", self.response.url)

        loader.add_css('cat_1', "ul.a-size-small > li:nth-child(1) > span:nth-child(1) > a:nth-child(1)::text")
        loader.add_css('cat_2', "ul.a-size-small > li:nth-child(3) > span:nth-child(1) > a:nth-child(1)::text")
        loader.add_css('cat_3', "ul.a-size-small > li:nth-child(5) > span:nth-child(1) > a:nth-child(1)::text")
        loader.add_css('cat_4', "ul.a-size-small > li:nth-child(7) > span:nth-child(1) > a:nth-child(1)::text")
        loader.add_css('rrp', "#price>table>tbody>tr>td.a-span12.a-color-secondary.a-size-base>span.a-text-strike::text")
        loader.add_css('num_reviews', "#acrCustomerReviewText::text")
        # loader.add_css('asin', "div#cerberus-data-metrics::attr(data-asin)")
        loader.add_css('answered_questions', "#askATFLink > span::text")

        if text is None:
            loader.add_css("price", ".olp-padding-right > span::text")

        if len(bullets) >= 1:
            for bullet in bullets:
                loader.add_value("bullets", bullet)

        """for each key value pair in info_dict create a field within dynamic item class,
            assign output processor and load items"""
        # if len(info_dict) != 0:
        # for k, v in zip(info_type, info):
        # if k is not None:
        # dynamicitem.fields[str(k)] = Field(output_processor=clean_text)
        # loader.add_value(str(k), str(v))
        fields = ["answered_questions", "rrp", "asin", "num_reviews",
                  "date_available", "brand", "weight", "dimensions",
                  "best_sellers_rank", "material", "ship_weight",
                  "model_number", "colour", "capacity", "volume_capacity",
                  "power", "auto_shutoff", "noise_level", "runtime",
                  "special_features", "reviews "]

        # if len(info_type) != 0:
        #     for k, v in zip(info_type, info):
        #         if k is not None:
        #             for x in fields:
        #                 if "_" in x:
        #                     y = x.split("_")
        #                     k = k.replace(" ", "").lower()
        #                     if y[0] in k and y[0] != "num":
        #                         loader.add_value(str(x), str(v))
        #                     elif y[1] in k and y[1] != "weight":
        #                         loader.add_value(str(x), str(v))
        #                 else:
        #                     k = k.replace(" ", "").lower()
        #                     if x in k:
        #                         loader.add_value(str(x), str(v))

        return loader

    def get_item_2(self, deal_name, option, text):

        bullets = []
        info = []
        info_type = []
        info_alt = []

        price = text

        if price is None:
            price = self.response.css("#priceblock_ourprice::text").extract_first()
            if price is None:
                price = self.response.css("#priceblock_dealprice::text").extract_first()
                if price is None:
                    price = self.response.css("#cerberus-data-metrics::attr(data-asin-price)").extract_first()
                    if price is None:
                        price = self.response.css("#unqualifiedBuyBox>div>div>span::text").extract_first()

        """pull information from details section and append key/value pairs to two lists"""
        for link in self.response.css("div.column>div>div>div>div>table>tbody>tr"):

            info.append(link.css("td.value::text").extract_first())
            info_type.append(link.css("td.label::text").extract_first())

        if len(info) == 0:

            for link in self.response.css("td.bucket>div.content>ul>li"):
                info_alt.append(link.css("::text").extract())

            for item in info_alt:
                info_type.append(item[0])
                info.append(item[1:])

        """pull bullet points from page and append to list"""
        for link in self.response.css("#feature-bullets>ul>li"):

            bullets.append(link.css("span::text").extract_first())

        prime_eligible = self.response.css("#bbop-check-box::attr(class)").extract_first()

        if prime_eligible is None:
            prime_eligible = self.response.css("#bundle-v2-atf-prime-logo::attr(class)").extract_first()

        if prime_eligible is not None:
            prime_eligible = True
        else:
            prime_eligible = False

        free_delivery = self.response.css("#shippingMessageInsideBuyBox_feature_div>div>div>div>span>span::text").extract_first()

        if free_delivery is not None:
            free_delivery = True
        else:
            free_delivery = self.response.css("#price-shipping-message > b::text").extract_first()
            if free_delivery is not None:
                free_delivery = True
            else:
                free_delivery = False

        reviews = self.response.css("#dp-summary-see-all-reviews > h2::text").extract_first()

        if reviews is None:
            reviews = self.response.css("#acrCustomerReviewText::text").extract_first()

        star_rating_5 = self.response.css("#histogramTable>tbody>tr:nth-child(1)>td:nth-child(3)>a::text").extract_first()
        star_rating_4 = self.response.css("#histogramTable>tbody>tr:nth-child(2)>td:nth-child(3)>a::text").extract_first()
        star_rating_3 = self.response.css("#histogramTable>tbody>tr:nth-child(3)>td:nth-child(3)>a::text").extract_first()
        star_rating_2 = self.response.css("#histogramTable>tbody>tr:nth-child(4)>td:nth-child(3)>a::text").extract_first()
        star_rating_1 = self.response.css("#histogramTable>tbody>tr:nth-child(5)>td:nth-child(3)>a::text").extract_first()

        if star_rating_1 is None:
            star_rating_1 = "0%"

        if star_rating_2 is None:
            star_rating_2 = "0%"

        if star_rating_3 is None:
            star_rating_3 = "0%"

        if star_rating_4 is None:
            star_rating_4 = "0%"

        if star_rating_5 is None:
            star_rating_5 = "0%"

        seller_name = self.response.css("#merchant-info > a::text").extract_first()

        if seller_name is None:
            seller_name = self.response.css("#bylineInfo::text").extract_first()

        if seller_name is None:
            seller_name = "Amazon"

        if prime_eligible is True \
                or seller_name is "Amazon":
            is_fba = True
        elif prime_eligible is False \
                or seller_name is not "Amazon":
            is_fba = False
        else:
            is_fba = "N/A"

        loader = AmazonItemLoader(selector=self.response)

        loader.add_value("deal_name", deal_name)
        loader.add_value("option", option)
        loader.add_value("price", price)
        loader.add_css("asin", "#cerberus-data-metrics::attr(data-asin)")
        loader.add_value("item_url", self.response.url)
        loader.add_value("prime_eligible", prime_eligible)
        loader.add_value("is_fba", is_fba)
        loader.add_value("free_delivery", free_delivery)
        loader.add_value("seller_name", seller_name)
        loader.add_value("num_reviews", reviews)

        loader.add_css("star_rating", ".arp-rating-out-of-text::text")
        loader.add_value("star_rating_5", star_rating_5)
        loader.add_value("star_rating_4", star_rating_4)
        loader.add_value("star_rating_3", star_rating_3)
        loader.add_value("star_rating_2", star_rating_2)
        loader.add_value("star_rating_1", star_rating_1)

        loader.add_css('cat_1', "ul.a-size-small > li:nth-child(1) > span:nth-child(1) > a:nth-child(1)::text")
        loader.add_css('cat_2', "ul.a-size-small > li:nth-child(3) > span:nth-child(1) > a:nth-child(1)::text")
        loader.add_css('cat_3', "ul.a-size-small > li:nth-child(5) > span:nth-child(1) > a:nth-child(1)::text")
        loader.add_css('cat_4', "ul.a-size-small > li:nth-child(7) > span:nth-child(1) > a:nth-child(1)::text")
        loader.add_css('rrp', "#price>table>tbody>tr>td.a-span12.a-color-secondary.a-size-base>span.a-text-strike::text")
        loader.add_css("answered_questions", "#askATFLink > span::text")

        if len(bullets) >= 1:
            for bullet in bullets:
                loader.add_value("bullets", bullet)

        fields = ["rrp", "date_available", "brand", "weight", "dimensions",
                  "best_sellers_rank", "material", "ship_weight",
                  "model_number", "colour", "capacity", "volume_capacity",
                  "power", "noise_level", "runtime", "special_features"]

        if len(info_type) != 0:

            for k, v in zip(info_type, info):

                if k is not None:

                    if "sellers" in k.lower():

                        loader.add_value("best_sellers_rank", str(v))

        if len(info_type) != 0:
            for k, v in zip(info_type, info):
                if k is not None:
                    for x in fields:
                        if "_" in x:
                            y = x.split("_")
                            k = k.replace(" ", "").lower()
                            if y[0] in k and y[0] != "num":
                                loader.add_value(str(x), str(v))
                            elif y[1] in k and y[1] != "weight":
                                loader.add_value(str(x), str(v))
                            elif "bestsellers" in k:
                                loader.add_value(str(x), str(v))
                        else:
                            k = k.replace(" ", "").lower()
                            if x in k:
                                loader.add_value(str(x), str(v))

        return loader

    def find_options(self):

        button_ids = []
        dropdown_ids = []
        button_number = 0
        dropdown_number = 0
        button_option_1_ids = []
        button_option_2_ids = []
        button_option_3_ids = []
        button_option_4_ids = []
        dd_option_1_ids = []
        dd_option_2_ids = []
        dd_option_3_ids = []
        dd_option_4_ids = []
        dd_options = [dd_option_1_ids, dd_option_2_ids, dd_option_3_ids, dd_option_4_ids]
        button_options = [button_option_1_ids, button_option_2_ids, button_option_3_ids, button_option_4_ids]

        for id in self.get_ids("#twister>div.a-section"):

            if id not in button_ids \
                    and "variation" in id:

                button_ids.append("#" + id + ">ul>li")
                dropdown_ids.append("#" + id + ">span>span>select>option")

        # Selector for installation options
        # button_ids.append("#vas-ppd-fake-twister>#vasTwisterRow>ul>li")

        print("BUTTON IDS : %r " % button_ids)
        print("DROPDOWN IDS : %r " % dropdown_ids)

        if button_ids is not None:

            if len(button_ids) == 1:

                for id in self.get_ids(button_ids[0]):

                    button_option_1_ids.append(id)

            elif len(button_ids) == 2:

                for id in self.get_ids(button_ids[0]):

                    button_option_1_ids.append(id)

                for id in self.get_ids(button_ids[1]):

                    button_option_2_ids.append(id)

            elif len(button_ids) == 3:

                for id in self.get_ids(button_ids[0]):

                    button_option_1_ids.append(id)

                for id in self.get_ids(button_ids[1]):

                    button_option_2_ids.append(id)

                for id in self.get_ids(button_ids[2]):

                    button_option_3_ids.append(id)

            elif len(button_ids) == 4:

                for id in self.get_ids(button_ids[0]):

                    button_option_1_ids.append(id)

                for id in self.get_ids(button_ids[1]):

                    button_option_2_ids.append(id)

                for id in self.get_ids(button_ids[2]):

                    button_option_3_ids.append(id)

                for id in self.get_ids(button_ids[3]):

                    button_option_4_ids.append(id)

        if dropdown_ids is not None:

            if len(dropdown_ids) == 1:

                for id in self.get_ids(dropdown_ids[0]):

                    dd_option_1_ids.append(id)

            elif len(dropdown_ids) == 2:

                for id in self.get_ids(dropdown_ids[0]):

                    dd_option_1_ids.append(id)

                for id in self.get_ids(dropdown_ids[1]):

                    dd_option_2_ids.append(id)

            elif len(dropdown_ids) == 3:

                for id in self.get_ids(dropdown_ids[0]):

                    dd_option_1_ids.append(id)

                for id in self.get_ids(dropdown_ids[1]):

                    dd_option_2_ids.append(id)

                for id in self.get_ids(dropdown_ids[2]):

                    dd_option_3_ids.append(id)

            elif len(dropdown_ids) == 4:

                for id in self.get_ids(dropdown_ids[0]):

                    dd_option_1_ids.append(id)

                for id in self.get_ids(dropdown_ids[1]):

                    dd_option_2_ids.append(id)

                for id in self.get_ids(dropdown_ids[2]):

                    dd_option_3_ids.append(id)

                for id in self.get_ids(dropdown_ids[3]):

                    dd_option_4_ids.append(id)

        for ids in button_options:

            if len(ids) >= 1:

                button_number += 1

        for ids in dd_options:

            if len(ids) >= 1:

                dropdown_number += 1

        print("DD OPTIONS 1 : %r " % dd_option_1_ids)
        print("DD OPTIONS 2 : %r " % dd_option_2_ids)
        print("DD OPTIONS 3 : %r " % dd_option_3_ids)
        print("DD OPTIONS 4 : %r " % dd_option_4_ids)
        print("BUTTON OPTIONS 1 : %r " % button_option_1_ids)
        print("BUTTON OPTIONS 2 : %r " % button_option_2_ids)
        print("BUTTON OPTIONS 3 : %r " % button_option_3_ids)
        print("BUTTON OPTIONS 4 : %r " % button_option_4_ids)
        print("DROPDOWN NUMBER : %r " % dropdown_number)
        print("BUTTON NUMBER : %r " % button_number)

        dd_options = [x for x in dd_options if len(x) >= 1]
        button_options = [x for x in button_options if len(x) >= 1]

        dropdown_keys = ["dd_option_1_ids", "dd_option_2_ids", "dd_option_3_ids", "dd_option_4_ids"]
        button_keys = ["button_option_1_ids", "button_option_2_ids", "button_option_3_ids", "button_option_4_ids"]
        button_dict = dict(zip(button_keys, button_options))
        dd_dict = dict(zip(dropdown_keys, dd_options))

        dict_values = [button_number, dropdown_number, button_dict, dd_dict]
        dict_keys = ["button_number", "dropdown_number", "button_dict", "dd_dict"]
        options = dict(zip(dict_keys, dict_values))

        return options
