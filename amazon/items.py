from scrapy.item import Item, Field
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Compose, MapCompose, Join, Identity
from datetime import datetime
import re

'''output processor for cleaning text based fields'''
clean_text = Compose(MapCompose(lambda v: v.strip()), Join())
clean_nums = MapCompose(lambda x: re.findall(r"\d+", str(x)))
clean_bool = Compose(MapCompose(lambda y: str(y)), Join())


def clean_option(value):

    x = value.replace("Click to select ", "")

    return x

output_processor=clean_nums
def clean_dims(value):

    """output processor for cleaning dimention field"""

    x = str(value.replace(" ", ""))
    x = str(x.replace(";", ","))
    x = str(x.replace("[", ""))
    x = str(x.replace("]", ""))
    x = x.replace("'", "")

    return x


def clean_date(value):

    """Output processor for correctly formatting dates"""
    y = []
    seperator = " "
    value = value.replace("'", "")
    value = value.replace('"', "")
    value = value.replace("[", "")
    value = value.replace("]", "")
    for x in value.split():
        if "Sept" in str(x):
            x = "Sep"
            y.append(x)
        if "." in str(x):
            x = x.replace(".", "")
            y.append(x)
        elif x not in y:
            y.append(x)

    if len(y[1]) > 3:
        y[1] = y[1][:3]

    y = seperator.join(y)
    x = datetime.strptime(str(y), "%d %b %Y").date()
    x = str(x)
    return x


def clean_price(value):

    """Output processor for cleaning numerical price fields"""

    dec = re.findall(r"\d+\.\d+", value)
    no_dec = re.findall(r"\d+", value)
    perc = re.findall(r"%", value)
    if len(perc) == 0:
        if len(dec) != 0:
            return dec
        else:
            return no_dec


def clean_price2(value):

    """Output processor for cleaning numerical price fields"""

    dec = re.findall(r"\d+\\,\d+", value)
    no_dec = re.findall(r"\d+", value)
    if len(dec) != 0:
        return dec
    else:
        return no_dec


def clean_path(value):

    """output processor for cleaning none values in path based fields
        possibly not needed here"""

    if value is not None:
        return value


def duplicates(value):

    """output processor for removing duplicate values"""
    val = []
    if value not in val:
        val.append(value)
    return val


class DebugItem(Item):

    url = Field()
    selector = Field()


class DebugItemLoader(ItemLoader):

    default_item_class = DebugItem


class AmazonIndexItem(Item):

    """An Item container for Amazon Products"""

    cat_0 = Field()
    cat_1 = Field()
    cat_2 = Field()
    cat_3 = Field()
    cat_4 = Field()
    cat_5 = Field()
    cat_6 = Field()
    cat_end = Field()
    item_url = Field()
    deal_name = Field()
    answered_questions = Field(output_processor=clean_nums)
    seller_name = Field()
    rrp = Field()
    price = Field()
    asin = Field(input_processor=clean_text)
    num_reviews = Field(output_processor=clean_bool)
    is_fba = Field(output_processor=clean_bool)
    date_available = Field(input_processor=clean_bool)
    brand = Field()
    weight = Field(input_processor=clean_bool)
    dimensions = Field(input_processor=clean_bool)
    best_sellers_rank = Field(input_processor=clean_text)
    material = Field()
    ship_weight = Field()
    model_number = Field()
    colour = Field()
    capacity = Field()
    volume_capacity = Field()
    power = Field()
    noise_level = Field()
    runtime = Field()
    special_features = Field()
    prime_eligible = Field(output_processor=clean_bool)
    option = Field()
    bullets = Field(input_processor=clean_text)
    star_rating = Field()
    star_rating_5 = Field(output_processor=clean_nums)
    star_rating_4 = Field(output_processor=clean_nums)
    star_rating_3 = Field(output_processor=clean_nums)
    star_rating_2 = Field(output_processor=clean_nums)
    star_rating_1 = Field(output_processor=clean_nums)
    free_delivery = Field(output_processor=clean_bool)


class AmazonItemLoader(ItemLoader):

    """Item Loader with output processors for Amazon Products"""

    default_item_class = AmazonIndexItem
    price_out = MapCompose(lambda x: clean_price(x))
    answered_questions_out = clean_text
    deal_name_out = clean_text
    cat_0_out = clean_text
    cat_1_out = clean_text
    cat_2_out = clean_text
    cat_3_out = clean_text
    cat_4_out = clean_text
    cat_5_out = clean_text
    cat_6_out = clean_text
    rrp_out = MapCompose(lambda x: clean_price(x))
    num_reviews = MapCompose(lambda x: clean_price2(x))
    asin_out = MapCompose(lambda x: duplicates(x))
    brand_out = clean_text
    model_number_out = MapCompose(lambda x: clean_dims(x))
    colour_out = clean_text
    weight_out = Compose(MapCompose(clean_text), clean_bool)
    dimensions_out = MapCompose(lambda x: clean_dims(x))
    material_out = clean_text
    best_sellers_rank_out = MapCompose(lambda x: clean_price2(x))
    ship_weight_out = clean_text
    date_available_out = MapCompose(lambda x: clean_date(x))
    seller_name_out = clean_text
    reviews_out = MapCompose(lambda x: clean_price2(x))
    bullets_out = clean_text
    option_out = MapCompose(lambda x: clean_option(x))
