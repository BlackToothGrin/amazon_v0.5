from urllib.parse import urlparse
from selenium.webdriver.support.expected_conditions import _find_element
allowed_params = ["node", "rh", "page"]


def format_url(url):
    # make sure URLs aren't relative, and strip unnecssary query args
    u = urlparse(url)

    scheme = u.scheme or "https"
    host = u.netloc or "www.amazon.co.uk"
    path = u.path

    if not u.query:
        query = ""
    else:
        query = "?"
        for piece in u.query.split("&"):
            k, v = piece.split("=")
            if k in allowed_params:
                query += "{k}={v}&".format(**locals())
        query = query[:-1]

    return "{scheme}://{host}{path}{query}".format(**locals())


# from .scrapy_framework import AmazonPage
# from .selenium_framework import SeleniumFramework


class Text_To_Change(object):
    def __init__(self, locator, text):
        self.locator = locator
        self.text = text

    def __call__(self, driver):
        actual_text = _find_element(driver, self.locator).text
        return actual_text != self.text


class TestClass:

    def __init__(self, name):

        self.name = name

    def print_name(self):

        return print(self.name.url)


class SizeColourPage:

    def __init__(self, response, size_ids, colour_ids):

        self.response = response
        self.size_ids = size_ids
        self.colour_ids = colour_ids

    def get_items(self):

        selenium = SeleniumFramework(self.response, headless=True)

        for x in self.size_ids:

            if "_-1" not in x:

                text, selector = selenium.find_price()

                selenium.click_option(x, selector)

            for y in self.colour_ids:

                colour_class = selenium.get_class(y)

                if "Unavailable" not in colour_class \
                        and "Select" not in colour_class:

                    text, selector = selenium.find_price()

                    text, stock = selenium.click_option(y, selector)

                    if stock is True:

                        text, selector = selenium.find_price()

                        size_option = selenium.get_text(x)
                        colour_option = selenium.get_title(y)

                        option = str(colour_option) + str(size_option)

                        deal_name = self.response.css("#productTitle::text").extract_first()

                        response = selenium.create_scrapy_response()

                        page = AmazonPage(response)

                        loader = page.get_item_2(deal_name, option, text)

                        yield loader.load_item()

                elif "Select" in colour_class:

                    stock = selenium.get_stock()

                    if stock is True:

                        text, selector = selenium.find_price()

                        size_option = selenium.get_text(x)
                        colour_option = selenium.get_title(y)

                        option = str(colour_option) + str(size_option)

                        deal_name = self.response.css("#productTitle::text").extract_first()

                        response = selenium.create_scrapy_response()

                        page = AmazonPage(response)

                        loader = page.get_item_2(deal_name, option, text)

                        yield loader.load_item()

        return selenium.close_driver()


class ColourStyleSizestylePage:

    def __init__(self, response, size_style_ids, colour_ids, style_ids):

        self.response = response
        self.size_style_ids = size_style_ids
        self.colour_ids = colour_ids
        self.style_ids = style_ids

    def get_items(self):

        selenium = SeleniumFramework(self.response, headless=True)

        for x in self.style_ids:

            text, selector = selenium.find_price()

            selenium.click_option(x, selector)

            for y in self.colour_ids:

                colour_class = selenium.get_class(y)

                if "Unavailable" not in colour_class:

                    text, selector = selenium.find_price()

                    selenium.click_option(y, selector)

                    for z in self.size_style_ids:

                        colour_class = selenium.get_class(z)

                        if "Unavailable" not in colour_class:

                            text, selector = selenium.find_price()

                            text, stock = selenium.click_option(z, selector)

                            if stock is True:

                                text, selector = selenium.find_price()

                                style_option = selenium.get_title(x)
                                colour_option = selenium.get_title(y)
                                size_option = selenium.get_title(z)

                                deal_name = self.response.css("#productTitle::text").extract_first()

                                option = str(colour_option) + str(style_option) + str(size_option)

                                response = selenium.create_scrapy_response()

                                page = AmazonPage(response)

                                loader = page.get_item_2(deal_name, option, text)

                                yield loader.load_item()

        return selenium.close_driver()


class ColourSizestylePage:

    def __init__(self, response, size_style_ids, colour_ids):

        self.response = response
        self.size_ids = size_style_ids
        self.colour_ids = colour_ids

    def get_items(self):

        selenium = SeleniumFramework(self.response, headless=True)

        for x in self.size_ids:

            text, selector = selenium.find_price()

            selenium.click_option(x, selector)

            for y in self.colour_ids:

                colour_class = selenium.get_class(y)

                if "Unavailable" not in colour_class \
                        and "Select" not in colour_class:

                    text, selector = selenium.find_price()

                    text, stock = selenium.click_option(y, selector)

                    if stock is True:

                        text, selector = selenium.find_price()

                        size_option = selenium.get_title(x)
                        colour_option = selenium.get_title(y)

                        option = str(colour_option) + str(size_option)

                        deal_name = self.response.css("#productTitle::text").extract_first()

                        response = selenium.create_scrapy_response()

                        page = AmazonPage(response)

                        loader = page.get_item_2(deal_name, option, text)

                        yield loader.load_item()

                elif "Select" in colour_class:

                    stock = selenium.get_stock()

                    if stock is True:

                        text, selector = selenium.find_price()

                        size_option = selenium.get_title(x)
                        colour_option = selenium.get_title(y)

                        option = str(colour_option) + str(size_option)

                        deal_name = self.response.css("#productTitle::text").extract_first()

                        response = selenium.create_scrapy_response()

                        page = AmazonPage(response)

                        loader = page.get_item_2(deal_name, option, text)

                        yield loader.load_item()

        return selenium.close_driver()


class ColourPage:

    def __init__(self, response, colour_ids):

        self.response = response
        self.colour_ids = colour_ids

    def get_items(self):

        selenium = SeleniumFramework(self.response, headless=True)

        for x in self.colour_ids:

            colour_class = selenium.get_class(x)
            colour_option = selenium.get_title(x)

            if "Unavailable" not in colour_class \
                    and "Select" not in colour_class:

                text, selector = selenium.find_price()

                text, stock = selenium.click_option(x, selector)

                if stock is True:

                    text, selector = selenium.find_price()

                    deal_name = self.response.css("#productTitle::text").extract_first()

                    option = str(colour_option)

                    response = selenium.create_scrapy_response()

                    page = AmazonPage(response)

                    loader = page.get_item_2(deal_name, option, text)

                    yield loader.load_item()

            elif "Select" in colour_class:

                stock = selenium.get_stock()

                if stock is True:

                    text, selector = selenium.find_price()

                    deal_name = self.response.css("#productTitle::text").extract_first()

                    option = str(colour_option)

                    response = selenium.create_scrapy_response()

                    page = AmazonPage(response)

                    loader = page.get_item_2(deal_name, option, text)

                    yield loader.load_item()

        return selenium.close_driver()


class SizePage:

    def __init__(self, response, size_ids):

        self.response = response
        self.colour_ids = size_ids

    def get_items(self):

        selenium = SeleniumFramework(self.response, headless=True)

        for x in self.colour_ids:

            colour_class = selenium.get_class(x)
            colour_option = selenium.get_text(x)

            if "Unavailable" not in colour_class \
                    and "Select" not in colour_class:

                text, selector = selenium.find_price()

                text, stock = selenium.click_option(x, selector)

                if stock is True:

                    text, selector = selenium.find_price()

                    option = str(colour_option)

                    deal_name = self.response.css("#productTitle::text").extract_first()

                    response = selenium.create_scrapy_response()

                    page = AmazonPage(response)

                    loader = page.get_item_2(deal_name, option, text)

                    yield loader.load_item()

            elif "Select" in colour_class:

                stock = selenium.get_stock()

                if stock is True:

                    text, selector = selenium.find_price()

                    option = str(colour_option)

                    deal_name = self.response.css("#productTitle::text").extract_first()

                    response = selenium.create_scrapy_response()

                    page = AmazonPage(response)

                    loader = page.get_item_2(deal_name, option, text)

                    yield loader.load_item()

        return selenium.close_driver()


class SizestyleStylePage(ColourSizestylePage):

    def __init__(self, response, size_style_ids, style_ids):

        self.response = response
        self.size_ids = size_style_ids
        self.colour_ids = style_ids


class SizestylePage:

    def __init__(self, response, size_style_ids):

        self.response = response
        self.colour_ids = size_style_ids

    def get_items(self):

        selenium = SeleniumFramework(self.response, headless=True)

        for x in self.colour_ids:

            colour_class = selenium.get_class(x)
            colour_option = selenium.get_text(x)

            if "Unavailable" not in colour_class:

                text, selector = selenium.find_price()

                text, stock = selenium.click_option(x, selector)

                if stock is True:

                    text, selector = selenium.find_price()

                    deal_name = self.response.css("#productTitle::text").extract_first()

                    option = str(colour_option)

                    response = selenium.create_scrapy_response()

                    page = AmazonPage(response)

                    loader = page.get_item_2(deal_name, option, text)

                    yield loader.load_item()

            elif "Select" in colour_class:

                stock = selenium.get_stock()

                if stock is True:

                    text, selector = selenium.find_price()

                    deal_name = self.response.css("#productTitle::text").extract_first()

                    option = str(colour_option)

                    response = selenium.create_scrapy_response()

                    page = AmazonPage(response)

                    loader = page.get_item_2(deal_name, option, text)

                    yield loader.load_item()

        return selenium.close_driver()


class ColourStylePage:

    def __init__(self, response, colour_style_ids):

        self.response = response
        self.colour_ids = colour_style_ids

    def get_items(self):

        selenium = SeleniumFramework(self.response, headless=True)

        for x in self.colour_ids:

            colour_class = selenium.get_class(x)
            colour_option = selenium.get_text(x)

            if "Unavailable" not in colour_class \
                    and "Select" not in colour_class:

                text, selector = selenium.find_price()

                text, stock = selenium.click_option(x, selector)

                if stock is True:

                    text, selector = selenium.find_price()

                    deal_name = self.response.css("#productTitle::text").extract_first()

                    option = str(colour_option)

                    response = selenium.create_scrapy_response()

                    page = AmazonPage(response)

                    loader = page.get_item_2(deal_name, option, text)

                    yield loader.load_item()

            elif "Select" in colour_class:

                stock = selenium.get_stock()

                if stock is True:

                    text, selector = selenium.find_price()

                    deal_name = self.response.css("#productTitle::text").extract_first()

                    option = str(colour_option)

                    response = selenium.create_scrapy_response()

                    page = AmazonPage(response)

                    loader = page.get_item_2(deal_name, option, text)

                    yield loader.load_item()

        return selenium.close_driver()


class ColourLensColourPage(ColourSizestylePage):

    def __init__(self, response, colour_ids, lens_colour_ids):

        self.response = response
        self.size_style_ids = colour_ids
        self.colour_ids = lens_colour_ids


class ColourandStylePage(ColourSizestylePage):

    def __init__(self, response, style_ids, colour_ids):

        self.response = response
        self.size_style_ids = style_ids
        self.colour_ids = colour_ids


class FlavourSizestylePage:

    def __init__(self, response, flavour_ids, size_style_ids):

        self.response = response
        self.flavour_ids = flavour_ids
        self.size_style_ids = size_style_ids

    def get_items(self):

        selenium = SeleniumFramework(self.response, headless=True)

        for x in self.flavour_ids:

            style_option = selenium.get_title(x)

            text, selector = selenium.find_price()

            selenium.click_option(x, selector)

            for y in self.size_style_ids:

                colour_class = selenium.get_class(y)
                colour_option = selenium.get_title(y)

                if "Unavailable" not in colour_class \
                        and "Select" not in colour_class:

                    text, selector = selenium.find_price()

                    text, stock = selenium.click_option(y, selector)

                    if stock is True:

                        text, selector = selenium.find_price()

                        deal_name = self.response.css("#productTitle::text").extract_first()

                        option = str(style_option) + str(colour_option)

                        response = selenium.create_scrapy_response()

                        page = AmazonPage(response)

                        loader = page.get_item_2(deal_name, option, text)

                        yield loader.load_item()

                elif "Select" in colour_class:

                    stock = selenium.get_stock()

                    if stock is True:

                        text, selector = selenium.find_price()

                        deal_name = self.response.css("#productTitle::text").extract_first()

                        option = str(style_option) + str(colour_option)

                        response = selenium.create_scrapy_response()

                        page = AmazonPage(response)

                        loader = page.get_item_2(deal_name, option, text)

                        yield loader.load_item()

        return selenium.close_driver()


class StyleDDSizestylePage:

    def __init__(self, response, style_dd_ids, size_style_ids):

        self.response = response
        self.style_dd_ids = style_dd_ids
        self.size_style_ids = size_style_ids

    def get_items(self):

        selenium = SeleniumFramework(self.response, headless=True)

        for x in self.style_dd_ids:

            style_option = selenium.get_text(x)

            text, selector = selenium.find_price()

            selenium.click_option(x, selector)

            for y in self.size_style_ids:

                size_class = selenium.get_class(y)
                size_option = selenium.get_title(y)

                if "Unavailable" not in size_class \
                        and "Select" not in size_class:

                    text, selector = selenium.find_price()

                    text, stock = selenium.click_option(y, selector)

                    if stock is True:

                        text, selector = selenium.find_price()

                        deal_name = self.response.css("#productTitle::text").extract_first()

                        option = str(style_option) + str(size_option)

                        response = selenium.create_scrapy_response()

                        page = AmazonPage(response)

                        loader = page.get_item_2(deal_name, option, text)

                        yield loader.load_item()

                elif "Select" in size_class:

                    stock = selenium.get_stock()

                    if stock is True:

                        text, selector = selenium.find_price()

                        deal_name = self.response.css("#productTitle::text").extract_first()

                        option = str(style_option) + str(size_option)

                        response = selenium.create_scrapy_response()

                        page = AmazonPage(response)

                        loader = page.get_item_2(deal_name, option, text)

                        yield loader.load_item()

        return selenium.close_driver()


class FlavourPage(ColourPage):

    def __init__(self, response, flavour_ids):

        self.response = response
        self.colour_ids = flavour_ids


class QuantityPage(ColourPage):

    def __init__(self, response, quantity_ids):

        self.response = response
        self.colour_ids = quantity_ids


class StylePage(ColourPage):

    def __init__(self, response, style_ids):

        self.response = response
        self.colour_ids = style_ids


class SizeColourDDPage:

    def __init__(self, response, size_ids, colour_dd_ids):

        self.response = response
        self.size_ids = size_ids
        self.colour_dd_ids = colour_dd_ids

    def get_items(self):

        selenium = SeleniumFramework(self.response, headless=True)

        for x in self.size_ids:

            if "_-1" not in x:

                text, selector = selenium.find_price()

                selenium.click_option(x, selector)

            for y in self.colour_dd_ids:

                colour_class = selenium.get_class(y)

                if "Unavailable" not in colour_class \
                        and "Select" not in colour_class:

                    text, selector = selenium.find_price()

                    text, stock = selenium.click_option(y, selector)

                    if stock is True:

                        text, selector = selenium.find_price()

                        size_option = selenium.get_text(x)
                        colour_option = selenium.get_text(y)

                        option = str(colour_option) + str(size_option)

                        deal_name = self.response.css("#productTitle::text").extract_first()

                        response = selenium.create_scrapy_response()

                        page = AmazonPage(response)

                        loader = page.get_item_2(deal_name, option, text)

                        yield loader.load_item()

                elif "Select" in colour_class:

                    stock = selenium.get_stock()

                    if stock is True:

                        text, selector = selenium.find_price()

                        size_option = selenium.get_text(x)
                        colour_option = selenium.get_text(y)

                        option = str(colour_option) + str(size_option)

                        deal_name = self.response.css("#productTitle::text").extract_first()

                        response = selenium.create_scrapy_response()

                        page = AmazonPage(response)

                        loader = page.get_item_2(deal_name, option, text)

                        yield loader.load_item()

        return selenium.close_driver()


class ColourDDPage:

    def __init__(self, response, colour_dd_ids):

        self.response = response
        self.colour_dd_ids = colour_dd_ids

    def get_items(self):

        selenium = SeleniumFramework(self.response, headless=True)

        for x in self.colour_dd_ids:

            colour_class = selenium.get_class(x)
            colour_option = selenium.get_text(x)

            if "Unavailable" not in colour_class \
                    and "Select" not in colour_class:

                text, selector = selenium.find_price()

                text, stock = selenium.click_option(x, selector)

                if stock is True:

                    text, selector = selenium.find_price()

                    option = str(colour_option)

                    deal_name = self.response.css("#productTitle::text").extract_first()

                    response = selenium.create_scrapy_response()

                    page = AmazonPage(response)

                    loader = page.get_item_2(deal_name, option, text)

                    yield loader.load_item()

            elif "Select" in colour_class:

                stock = selenium.get_stock()

                if stock is True:

                    text, selector = selenium.find_price()

                    option = str(colour_option)

                    deal_name = self.response.css("#productTitle::text").extract_first()

                    response = selenium.create_scrapy_response()

                    page = AmazonPage(response)

                    loader = page.get_item_2(deal_name, option, text)

                    yield loader.load_item()

        return selenium.close_driver()


class ColourAndStylePage(ColourSizestylePage):

    def __init__(self, response, style_ids, colour_ids):

        self.response = response
        self.size_style_ids = style_ids
        self.colour_ids = colour_ids


class ScentPage(ColourPage):

    def __init__(self, response, scent_ids):

        self.response = response
        self.colour_ids = scent_ids


class PatternPage(ColourPage):

    def __init__(self, response, pattern_ids):

        self.response = response
        self.colour_ids_ids = pattern_ids


class MaterialSizestylePage:

    def __init__(self, response, material_dd_ids, size_style_ids):

        self.response = response
        self.material_dd_ids = material_dd_ids
        self.size_style_ids = size_style_ids

    def get_items(self):

        selenium = SeleniumFramework(self.response, headless=True)

        for x in self.material_dd_ids:

            if "_-1" not in x:

                text, selector = selenium.find_price()

                selenium.click_option(x, selector)

            for y in self.size_style_ids:

                size_style_class = selenium.get_class(y)

                if "Unavailable" not in size_style_class \
                        and "Select" not in size_style_class:

                    text, selector = selenium.find_price()

                    text, stock = selenium.click_option(y, selector)

                    if stock is True:

                        text, selector = selenium.find_price()

                        material_option = selenium.get_text(x)
                        size_style_option = selenium.get_title(y)

                        option = str(material_option) + str(size_style_option)

                        deal_name = self.response.css("#productTitle::text").extract_first()

                        response = selenium.create_scrapy_response()

                        page = AmazonPage(response)

                        loader = page.get_item_2(deal_name, option, text)

                        yield loader.load_item()

                elif "Select" in size_style_class:

                    stock = selenium.get_stock()

                    if stock is True:

                        text, selector = selenium.find_price()

                        material_option = selenium.get_text(x)
                        size_style_option = selenium.get_title(y)

                        option = str(material_option) + str(size_style_option)

                        deal_name = self.response.css("#productTitle::text").extract_first()

                        response = selenium.create_scrapy_response()

                        page = AmazonPage(response)

                        loader = page.get_item_2(deal_name, option, text)

                        yield loader.load_item()

        return selenium.close_driver()


class DisplayheightColourPage(ColourSizestylePage):

    def __init__(self, response, display_height_ids, colour_ids):

        self.response = response
        self.size_style_ids = display_height_ids
        self.colour_ids = colour_ids


class QuantityColourPage(ColourSizestylePage):

    def __init__(self, response, quantity_ids, colour_ids):

        self.response = response
        self.quantity_ids = quantity_ids
        self.colour_ids = colour_ids


class WattagePage(ColourPage):

    def __init__(self, response, wattage_ids):

        self.response = response
        self.colour_ids = wattage_ids


class InstallationPage(ColourPage):

    def __init__(self, response, installation_ids):

        self.response = response
        self.colour_ids = installation_ids
