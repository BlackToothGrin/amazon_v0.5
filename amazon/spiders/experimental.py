from scrapy.spiders import Spider
from scrapy.http import Request
from ..selenium_framework import OptionsClicker
from ..scrapy_framework import AmazonPage
import itertools
import sentry_sdk
sentry_sdk.init("https://f3e228efcb7644b5844470655976219a@sentry.io/1423232")


class ExperimentalSpider(Spider):
    name = 'experimental'
    allowed_domains = ['amazon.co.uk']
    custom_settings = {
        'ITEM_PIPELINES': {
            'amazon.pipelines.CsvPipeline': 900,
        }
    }
    start_urls = [
        "https://www.amazon.co.uk/Zusue-Comfort-Stretch-Breathable-Suitable/dp/B07PR6H1BG"
        # "https://www.amazon.co.uk/Samsung-UE43NU7400-43-Inch-Dynamic-Certified/dp/B07D769GTS"
        # "https://www.amazon.co.uk/Btruely-Womens-Ruffles-T-Shirt-Chiffon/dp/B07PLNBRC8"
        # "https://www.amazon.co.uk/Loofah-Exfoliating-Scrubber-Massaging-Shower/dp/B07KF7G7W5"
        # "https://www.amazon.co.uk/Fruit-Loom-Unisex-Short-Sleeve/dp/B0171SM8LM"
        # "https://www.amazon.co.uk/AmazonBasics-Microfibre-Fitted-Sheet-Double/dp/B06XFFB9TZ"
        # "https://www.amazon.co.uk/Blooming-Jelly-Ladies-Sleeve-Dresses/dp/B07HJZFGC9"
    ]

# "https://www.amazon.co.uk/AmazonBasics-Microfibre-Fitted-Sheet-Double/dp/B06XFFB9TZ",
# "https://www.amazon.co.uk/KINGBO-Spectrum-Greenhouse-Hydroponic-Growing/dp/B07BSQZL99/ref=lp_3764803031_1_17?s=lighting&ie=UTF8&qid=1553689011&sr=1-17",
# "https://www.amazon.co.uk/ENKNIGHT-Women%60s-Shoulder-Messenger-Handbag/dp/B0773F8XTK",
# "https://www.amazon.co.uk/Container-Refillable-Squeezable-Cosmetics-Conditioner/dp/B07HF1ZS3K"
# "https://www.amazon.co.uk/Ceiling-Control-Polyester-Replaceable-Lighting/dp/B07123G267/ref=lp_3764799031_1_32_sspa?s=lighting&ie=UTF8&qid=1553689038&sr=1-32-spons&psc=1"]
# "https://www.amazon.co.uk/KINGBO-Spectrum-Greenhouse-Hydroponic-Growing/dp/B07BSQZL99/ref=lp_3764803031_1_17?s=lighting&ie=UTF8&qid=1553689011&sr=1-17"]
# "https://www.amazon.co.uk/Loofah-Exfoliating-Scrubber-Massaging-Shower/dp/B07KF7G7W5/ref=lp_3091968031_1_51_m/260-4726681-7786740?s=beauty&ie=UTF8&qid=1553594334&sr=1-51",
# "https://www.amazon.co.uk/drawstring-natural-Exfoliating-Drawstring-Bathing/dp/B07MZQWDCV/ref=lp_3091968031_1_17_m/260-4726681-7786740?s=beauty&ie=UTF8&qid=1553594334&sr=1-17"
# "https://www.amazon.co.uk/Beauty-Tools-Accessories/b?ie=UTF8&node=3474291031"]
# "https://www.amazon.co.uk/Container-Refillable-Squeezable-Cosmetics-Conditioner/dp/B07HF1ZS3K",
# "https://www.amazon.co.uk/ENKNIGHT-Women%60s-Shoulder-Messenger-Handbag/dp/B0773F8XTK",
# "https://www.amazon.co.uk/Kipling-Womens-New-Hiphurray-Tote/dp/B07L3JKTM7",
# "https://www.amazon.co.uk/XYCSZQ-Leather-Rubber-Comfortable-Breathable/dp/B07HBQK83S",
# "https://www.amazon.co.uk/Baby-Banz-Retro-years-Sunglasses/dp/B001DCVEZY",
# "https://www.amazon.co.uk/Fruit-Loom-Unisex-Short-Sleeve/dp/B0171SM8LM",
# "https://www.amazon.co.uk/Dickies-08-440063-Fonda-Beanie/dp/B07HZGS2RH",
# "https://www.amazon.co.uk/Duerer-Compression-Rheumatiod-Tendonitis-Fingerless/dp/B07MYYN8B8",
# "https://www.amazon.co.uk/Levis-Batwing-Outline-Flexfit-Baseball/dp/B07PT8ZJ33",
# "https://www.amazon.co.uk/Duerer-Compression-Rheumatiod-Tendonitis-Fingerless/dp/B07MYYN8B8",
# "https://www.amazon.co.uk/Baby-Banz-Retro-years-Sunglasses/dp/B001DCVEZY",
# "https://www.amazon.co.uk/Childrens-Classic-Sunglasses-Protection-Unisex-x/dp/B016E3ZFDI",
# "https://www.amazon.co.uk/womens-swimwear-bikinis/b?ie=UTF8&node=1731490031",
# "https://www.amazon.co.uk/TACVASEN-Military-Combat-T-Shirt-Pockets/dp/B07BY9LPQD",
# "https://www.amazon.co.uk/Tommy-Hilfiger-Reversible-Bomber-Jacket/dp/B07L3H2BCC",
# "https://www.amazon.co.uk/Tommy-Hilfiger-Womens-Chiara-Sleeve/dp/B00G4QM8UE",
# "https://www.amazon.co.uk/Extractor-Neoteck-Adjustment-Optical-Converter-Green/dp/B07D7M8LDN",
# "https://www.amazon.co.uk/LG-BP250-Up-scaling-external-playback/dp/B00TWW583Q",
# "https://www.amazon.co.uk/LED-Smart-4K-TVs/b?ie=UTF8&node=560864",
# "https://www.amazon.co.uk/mp3-ipod-headphones-DAB-radio/b?ie=UTF8&node=560884",
# "https://www.amazon.co.uk/LED-Smart-4K-TVs/b?ie=UTF8&node=560864",
# "https://www.amazon.co.uk/Electronics-Accessories/b?ie=UTF8&node=1345741031",
# "https://www.amazon.co.uk/Bookshelf-Hi-Fi-Speakers/b?ie=UTF8&node=199618031",
# "https://www.amazon.co.uk/electronics-camera-mp3-ipod-tv/b?ie=UTF8&node=560798",
# "https://www.amazon.co.uk/SoundTouch-Bluetooth-Wi-Fi-Speaker-Generation/dp/B073D4R222"

    def start_requests(self):

        for url in self.start_urls:

            request = Request(url, self.parse_items)
            yield request

    def parse(self, response):

        """Parse function will iterate through each selector style to find urls to follow"""

        page = AmazonPage(response)
        url_list = page.get_categories()

        if len(url_list) != 0:

            for url in url_list:

                request = page.generate_request(url, self.parse)
                yield request

        else:

            requests = page.get_item_requests(self.parse_items)

            for request in requests:

                yield request

            request = page.paginate(self.parse_items)
            yield request

    def parse_items(self, response):
        """Parse function for product pages: extracts information from the page
            and passes scraped items through custom ItemLoader for cleaning"""

        self.log("I just visited : " + response.url)

        page = AmazonPage(response)

        options = page.find_options()

        print("OPTIONS DICTIONARY : %r " % options)

        button_dict = options['button_dict']
        dd_dict = options['dd_dict']

        bo_1 = []
        bo_2 = []
        bo_3 = []
        bo_4 = []
        ddo_1 = []
        ddo_2 = []
        ddo_3 = []
        ddo_4 = []

        if options['button_number'] == 1:

            bo_1 = [x for x in button_dict['button_option_1_ids']]

        elif options['button_number'] == 2:

            bo_1 = [x for x in button_dict['button_option_1_ids']]
            bo_2 = [x for x in button_dict['button_option_2_ids']]

        elif options['button_number'] == 3:

            bo_1 = [x for x in button_dict['button_option_1_ids']]
            bo_2 = [x for x in button_dict['button_option_2_ids']]
            bo_3 = [x for x in button_dict['button_option_3_ids']]

        elif options['button_number'] == 4:

            bo_1 = [x for x in button_dict['button_option_1_ids']]
            bo_2 = [x for x in button_dict['button_option_2_ids']]
            bo_3 = [x for x in button_dict['button_option_3_ids']]
            bo_4 = [x for x in button_dict['button_option_4_ids']]

        if options['dropdown_number'] == 1:

            ddo_1 = [x for x in dd_dict['dd_option_1_ids']]

        elif options['dropdown_number'] == 2:

            ddo_1 = [x for x in dd_dict['dd_option_1_ids']]
            ddo_2 = [x for x in dd_dict['dd_option_2_ids']]

        elif options['dropdown_number'] == 3:

            ddo_1 = [x for x in dd_dict['dd_option_1_ids']]
            ddo_2 = [x for x in dd_dict['dd_option_2_ids']]
            ddo_3 = [x for x in dd_dict['dd_option_3_ids']]

        elif options['dropdown_number'] == 4:

            ddo_1 = [x for x in dd_dict['dd_option_1_ids']]
            ddo_2 = [x for x in dd_dict['dd_option_2_ids']]
            ddo_3 = [x for x in dd_dict['dd_option_3_ids']]
            ddo_4 = [x for x in dd_dict['dd_option_4_ids']]

        button_list = [bo_1, bo_2, bo_3, bo_4]
        dd_list = [ddo_1, ddo_2, ddo_3, ddo_4]

        print("BUTTON OPTIONS LIST : %r " % button_list)
        print("DROPDOWN OPTIONS LIST : %r " % dd_list)

        if options['button_number'] == 1 \
                and options['dropdown_number'] == 0:

            print("======================================================")
            print("======================================================")
            print("======================================================")
            print("               SINGLE BUTTON OPTIONS                  ")
            print("======================================================")
            print("======================================================")
            print("======================================================")

            selenium = OptionsClicker(response)

            for item in selenium.get_single_button_items(bo_1):

                yield item

        elif options['button_number'] == 2 \
                and options['dropdown_number'] == 0:

            print("======================================================")
            print("======================================================")
            print("======================================================")
            print("               DOUBLE BUTTON OPTIONS                  ")
            print("======================================================")
            print("======================================================")
            print("======================================================")

            selenium = OptionsClicker(response)

            for item in selenium.get_double_button_items(bo_1, bo_2):

                yield item

        elif options['button_number'] == 3 \
                and options['dropdown_number'] == 0:

            print("======================================================")
            print("======================================================")
            print("======================================================")
            print("               TRIPLE BUTTON OPTIONS                  ")
            print("======================================================")
            print("======================================================")
            print("======================================================")

            selenium = OptionsClicker(response)

            for item in selenium.get_triple_button_items(bo_1, bo_2, bo_3):

                yield item

        elif options['button_number'] == 0 \
                and options['dropdown_number'] == 1:

            print("======================================================")
            print("======================================================")
            print("======================================================")
            print("               SINGLE DROPDOWN OPTIONS                ")
            print("======================================================")
            print("======================================================")
            print("======================================================")

            selenium = OptionsClicker(response)

            for item in selenium.get_single_dd_items(ddo_1):

                yield item

        elif options['button_number'] == 0 \
                and options['dropdown_number'] == 2:

            print("======================================================")
            print("======================================================")
            print("======================================================")
            print("               DOUBLE DROPDOWN OPTIONS                ")
            print("======================================================")
            print("======================================================")
            print("======================================================")

            selenium = OptionsClicker(response)

            for item in selenium.get_double_dd_items(ddo_1, ddo_2):

                yield item

        elif options['button_number'] == 0 \
                and options['dropdown_number'] == 3:

            print("======================================================")
            print("======================================================")
            print("======================================================")
            print("               TRIPLE DROPDOWN OPTIONS                ")
            print("======================================================")
            print("======================================================")
            print("======================================================")

            selenium = OptionsClicker(response)

            for item in selenium.get_triple_dd_items(ddo_1, ddo_2, ddo_3):

                yield item

        elif options['button_number'] == 1 \
                and options['dropdown_number'] == 1:

            print("======================================================")
            print("======================================================")
            print("======================================================")
            print("         SINGLE DD & SINGLE BUTTON OPTIONS            ")
            print("======================================================")
            print("======================================================")
            print("======================================================")

            selenium = OptionsClicker(response)

            for item in selenium.get_single_button_single_dd_items(bo_1, ddo_1):

                yield item

        elif options['button_number'] == 2 \
                and options['dropdown_number'] == 1:

            print("======================================================")
            print("======================================================")
            print("======================================================")
            print("         SINGLE DD & DOUBLE BUTTON OPTIONS            ")
            print("======================================================")
            print("======================================================")
            print("======================================================")

            selenium = OptionsClicker(response)

            for item in selenium.get_single_dd_double_button_items(ddo_1, bo_1, bo_2):

                yield item

        elif options['button_number'] == 0 \
                and options['dropdown_number'] == 0:

            print("======================================================")
            print("======================================================")
            print("======================================================")
            print("                 NO OPTIONS                           ")
            print("======================================================")
            print("======================================================")
            print("======================================================")

            deal_name = response.css("#productTitle::text").extract_first()
            option = "FLAG"
            text = response.css("#cerberus-data-metrics::attr(data-asin-price)").extract_first()

            loader = page.get_item_2(deal_name, option, text)
            return loader.load_item()
