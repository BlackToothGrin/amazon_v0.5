from scrapy.spiders import Spider
from scrapy.http import Request
from ..selenium_framework import OptionsClicker
from ..scrapy_framework import AmazonPage
import itertools
import sentry_sdk
sentry_sdk.init("https://f3e228efcb7644b5844470655976219a@sentry.io/1423232")


class SmallSpider(Spider):
    name = 'small_test'
    allowed_domains = ['amazon.co.uk']
    custom_settings = {
        'ITEM_PIPELINES': {
            'amazon.pipelines.CsvPipeline': 900,            # Turn this off for production/production testing
        }
    }
    start_urls = [
        # "https://www.amazon.co.uk/Zusue-Comfort-Stretch-Breathable-Suitable/dp/B07PR6H1BG",
        "https://www.amazon.co.uk/Libertine-Mens-Hunter-Dress-Shirt/dp/B07HBK9KC2/ref=pd_d0_recs_v2_nib_1/261-8798441-8558460?_encoding=UTF8&pd_rd_i=B07G6HKT7T&pd_rd_r=c0103ba0-55ed-11e9-b85e-213a7bc01ca2&pd_rd_w=mN51m&pd_rd_wg=ujn0u&pf_rd_p=c35be2a8-b085-4654-b878-f34ce9d6deac&pf_rd_r=94BKANTDV74ZGB330PVY&refRID=94BKANTDV74ZGB330PVY&th=1"
    ]

    def start_requests(self):
        for url in self.start_urls:                         # Leave this alone on Small_Test
            request = Request(url, self.parse_items)
            yield request

    def parse(self, response):

        """Parse function will iterate through each selector style to find urls to follow"""

        page = AmazonPage(response)
        url_list = page.get_categories()

        if len(url_list) != 0:

            for url in url_list:

                request = page.generate_request(url, self.parse)
                yield request

        else:

            requests = page.get_item_requests(self.parse_items)

            for request in requests:

                yield request

            # request = page.paginate(self.parse_items)
            # yield request

    def parse_items(self, response):
        """Parse function for product pages: extracts information from the page
            and passes scraped items through custom ItemLoader for cleaning"""

        self.log("I just visited : " + response.url)

        page = AmazonPage(response)

        options = page.find_options()

        print("OPTIONS DICTIONARY : %r " % options)

        button_dict = options['button_dict']
        dd_dict = options['dd_dict']

        bo_1 = []
        bo_2 = []
        bo_3 = []
        bo_4 = []
        ddo_1 = []
        ddo_2 = []
        ddo_3 = []
        ddo_4 = []

        if options['button_number'] == 1:

            bo_1 = [x for x in button_dict['button_option_1_ids']]

        elif options['button_number'] == 2:

            bo_1 = [x for x in button_dict['button_option_1_ids']]
            bo_2 = [x for x in button_dict['button_option_2_ids']]

        elif options['button_number'] == 3:

            bo_1 = [x for x in button_dict['button_option_1_ids']]
            bo_2 = [x for x in button_dict['button_option_2_ids']]
            bo_3 = [x for x in button_dict['button_option_3_ids']]

        elif options['button_number'] == 4:

            bo_1 = [x for x in button_dict['button_option_1_ids']]
            bo_2 = [x for x in button_dict['button_option_2_ids']]
            bo_3 = [x for x in button_dict['button_option_3_ids']]
            bo_4 = [x for x in button_dict['button_option_4_ids']]

        if options['dropdown_number'] == 1:

            ddo_1 = [x for x in dd_dict['dd_option_1_ids']]

        elif options['dropdown_number'] == 2:

            ddo_1 = [x for x in dd_dict['dd_option_1_ids']]
            ddo_2 = [x for x in dd_dict['dd_option_2_ids']]

        elif options['dropdown_number'] == 3:

            ddo_1 = [x for x in dd_dict['dd_option_1_ids']]
            ddo_2 = [x for x in dd_dict['dd_option_2_ids']]
            ddo_3 = [x for x in dd_dict['dd_option_3_ids']]

        elif options['dropdown_number'] == 4:

            ddo_1 = [x for x in dd_dict['dd_option_1_ids']]
            ddo_2 = [x for x in dd_dict['dd_option_2_ids']]
            ddo_3 = [x for x in dd_dict['dd_option_3_ids']]
            ddo_4 = [x for x in dd_dict['dd_option_4_ids']]

        button_list = [bo_1, bo_2, bo_3, bo_4]
        dd_list = [ddo_1, ddo_2, ddo_3, ddo_4]

        print("BUTTON OPTIONS LIST : %r " % button_list)
        print("DROPDOWN OPTIONS LIST : %r " % dd_list)

        if options['button_number'] == 1 \
                and options['dropdown_number'] == 0:

            print("======================================================")
            print("======================================================")
            print("======================================================")
            print("               SINGLE BUTTON OPTIONS                  ")
            print("======================================================")
            print("======================================================")
            print("======================================================")

            selenium = OptionsClicker(response)

            for item in selenium.get_single_button_items(bo_1):

                yield item

        elif options['button_number'] == 2 \
                and options['dropdown_number'] == 0:

            print("======================================================")
            print("======================================================")
            print("======================================================")
            print("               DOUBLE BUTTON OPTIONS                  ")
            print("======================================================")
            print("======================================================")
            print("======================================================")

            selenium = OptionsClicker(response)

            for item in selenium.get_double_button_items(bo_1, bo_2):

                yield item

        elif options['button_number'] == 3 \
                and options['dropdown_number'] == 0:

            print("======================================================")
            print("======================================================")
            print("======================================================")
            print("               TRIPLE BUTTON OPTIONS                  ")
            print("======================================================")
            print("======================================================")
            print("======================================================")

            selenium = OptionsClicker(response)

            for item in selenium.get_triple_button_items(bo_1, bo_2, bo_3):

                yield item

        elif options['button_number'] == 0 \
                and options['dropdown_number'] == 1:

            print("======================================================")
            print("======================================================")
            print("======================================================")
            print("               SINGLE DROPDOWN OPTIONS                ")
            print("======================================================")
            print("======================================================")
            print("======================================================")

            selenium = OptionsClicker(response)

            for item in selenium.get_single_dd_items(ddo_1):

                yield item

        elif options['button_number'] == 0 \
                and options['dropdown_number'] == 2:

            print("======================================================")
            print("======================================================")
            print("======================================================")
            print("               DOUBLE DROPDOWN OPTIONS                ")
            print("======================================================")
            print("======================================================")
            print("======================================================")

            selenium = OptionsClicker(response)

            for item in selenium.get_double_dd_items(ddo_1, ddo_2):

                yield item

        elif options['button_number'] == 0 \
                and options['dropdown_number'] == 3:

            print("======================================================")
            print("======================================================")
            print("======================================================")
            print("               TRIPLE DROPDOWN OPTIONS                ")
            print("======================================================")
            print("======================================================")
            print("======================================================")

            selenium = OptionsClicker(response)

            for item in selenium.get_triple_dd_items(ddo_1, ddo_2, ddo_3):

                yield item

        elif options['button_number'] == 1 \
                and options['dropdown_number'] == 1:

            print("======================================================")
            print("======================================================")
            print("======================================================")
            print("         SINGLE DD & SINGLE BUTTON OPTIONS            ")
            print("======================================================")
            print("======================================================")
            print("======================================================")

            selenium = OptionsClicker(response)

            for item in selenium.get_single_button_single_dd_items(bo_1, ddo_1):

                yield item

        elif options['button_number'] == 2 \
                and options['dropdown_number'] == 1:

            print("======================================================")
            print("======================================================")
            print("======================================================")
            print("         SINGLE DD & DOUBLE BUTTON OPTIONS            ")
            print("======================================================")
            print("======================================================")
            print("======================================================")

            selenium = OptionsClicker(response)

            for item in selenium.get_single_dd_double_button_items(ddo_1, bo_1, bo_2):

                yield item

        elif options['button_number'] == 1 \
                and options['dropdown_number'] == 2:

            print("======================================================")
            print("======================================================")
            print("======================================================")
            print("         DOUBLE DD & SINGLE BUTTON OPTIONS            ")
            print("======================================================")
            print("======================================================")
            print("======================================================")

            selenium = OptionsClicker(response)

            for item in selenium.get_double_dd_single_button_items(ddo_1, ddo_2, bo_1):

                yield item

        elif options['button_number'] == 0 \
                and options['dropdown_number'] == 0:

            print("======================================================")
            print("======================================================")
            print("======================================================")
            print("                 NO OPTIONS                           ")
            print("======================================================")
            print("======================================================")
            print("======================================================")

            deal_name = response.css("#productTitle::text").extract_first()
            option = "NO OPTIONS"
            text = response.css("#cerberus-data-metrics::attr(data-asin-price)").extract_first()

            loader = page.get_item_2(deal_name, option, text)
            return loader.load_item()
