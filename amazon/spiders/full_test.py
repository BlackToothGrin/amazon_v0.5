# -*- coding: utf-8 -*-
from scrapy.spiders import Spider
from scrapy.http import Request
from ..helpers import *
from ..scrapy_framework import AmazonPage
from ..items import DebugItemLoader
import itertools
import sentry_sdk
sentry_sdk.init("https://f3e228efcb7644b5844470655976219a@sentry.io/1423232")


class FullTestSpider(Spider):
    name = 'full_test'
    allowed_domains = ['amazon.co.uk']
    start_urls = [
        "https://www.amazon.co.uk/b/ref=sd_allcat_prime_pantry_fc?ie=UTF8&node=5782663031",
        "https://www.amazon.co.uk/b/ref=sd_allcat_prime_pantry_bv?ie=UTF8&node=5782664031",
        "https://www.amazon.co.uk/b/ref=sd_allcat_prime_pantry_bws?ie=UTF8&node=8464529031",
        "https://www.amazon.co.uk/b/ref=sd_allcat_prime_pantry_bc?ie=UTF8&node=8479375031",
        "https://www.amazon.co.uk/b/ref=sd_allcat_prime_pantry_hb?ie=UTF8&node=5790355031",
        "https://www.amazon.co.uk/b/ref=sd_allcat_prime_pantry_hs?ie=UTF8&node=5790354031",
        "https://www.amazon.co.uk/b/ref=sd_allcat_prime_pantry_pt?ie=UTF8&node=5790353031",
        "https://www.amazon.co.uk/Sportswear-Outdoor-Clothing/b/ref=sd_allcat_aocl?ie=UTF8&node=116189031",
        "https://www.amazon.co.uk/Exercise-Fitness-Toning-Strength-Equipment/b/ref=sd_allcat_exf?ie=UTF8&node=319535011",
        "https://www.amazon.co.uk/beauty-cosmetics/b/ref=sd_allcat_bty?ie=UTF8&node=117332031",
        "https://www.amazon.co.uk/toys/b/ref=sd_allcat_tg?ie=UTF8&node=468292",
        "https://www.amazon.co.uk/home-garden-kitchen-appliances-lighting/b/ref=sd_allcat_hg_t2?ie=UTF8&node=11052591"
    ]

    def start_requests(self):

        for url in self.start_urls:

            request = Request(url, self.parse)
            yield request

    def parse(self, response):

        """Parse function will iterate through each selector style to find urls to follow"""

        page = AmazonPage(response)
        url_list = page.get_categories()

        if len(url_list) != 0:

            for url in url_list:

                request = page.generate_request(url, self.parse)
                yield request

        else:

            requests = page.get_item_requests(self.parse_items)

            for request in requests:

                yield request

            request = page.paginate(self.parse_items)
            yield request

    def parse_items(self, response):
        """Parse function for product pages: extracts information from the page
            and passes scraped items through custom ItemLoader for cleaning"""

        self.log("I just visited : " + response.url)

        page = AmazonPage(response)

        styles_list = []

        styles = page.find_styles(styles_list)

        for style in styles:

            loader = DebugItemLoader(response)

            loader.add_value("url", response.url)
            loader.add_value("selector", style)

            yield loader.load_item()