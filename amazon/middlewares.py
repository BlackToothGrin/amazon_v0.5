# -*- coding: utf-8 -*-

# Define here the models for your spider middleware
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/spider-middleware.html

from scrapy import signals
from base64 import b64encode, b64decode
from .settings import BOTPROXY_USER, BOTPROXY_PASSWORD

user = BOTPROXY_USER
password = BOTPROXY_PASSWORD


# Start your middleware class
class ProxyMiddleware(object):
    # overwrite process request
    def process_request(self, request, spider):
        # Set the location of the proxy
        request.meta['proxy'] = "http://x.botproxy.net:8443"

        # Use the following lines if your proxy requires authentication
        auth_creds = user + ":" + password
        print("AUTH_CREDS : %r" % auth_creds)
        # setup basic authentication for the proxy
        access_token = b64decode(bytes(auth_creds, "ascii"))
        request.headers['Proxy-Authorization'] = 'Basic ' + access_token
