from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, TimeoutException, StaleElementReferenceException, WebDriverException, ElementNotVisibleException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import _find_element, element_to_be_clickable, visibility_of_element_located, element_located_to_be_selected
from scrapy.http import HtmlResponse
from .scrapy_framework import AmazonPage
import time
driver_path = r"/home/blacktooth/projects/scraping/amazon/chromedriver"
#Driver path HAS TO BE the ABSOLUTE path to chromedriver within ec2


class Text_To_Change(object):

    def __init__(self, locator, text):
        self.locator = locator
        self.text = text

    def __call__(self, driver):
        actual_text = _find_element(driver, self.locator).text
        return actual_text != self.text


class SeleniumFramework:

    def __init__(self, response, headless=False):

        self.response = response
        chromeoptions = webdriver.ChromeOptions()
        prefs = {'profile.managed_default_content_settings.images': 2}
        chromeoptions.add_experimental_option("prefs", prefs)
        if headless is True:
            chromeoptions.add_argument("--headless")
        self.driver = webdriver.Chrome(executable_path=driver_path, options=chromeoptions)
        self.driver.get(self.response.url)

    def get_class(self, id):

        text = self.driver.find_element_by_id(str(id)).get_attribute("class")

        return text

    def get_title(self, id):

        text = self.driver.find_element_by_id(str(id)).get_attribute("title")

        return text

    def get_text(self, id):

        text = self.driver.find_element_by_id(str(id)).get_attribute("text")

        return text

    def get_text_by_css(self, css):

        text = self.driver.find_element_by_css_selector(css).text

        return text

    def get_attribute_by_id(self, id, attribute):

        text = self.driver.find_element_by_id(id).get_attribute(attribute)

        return text

    def close_driver(self):

        return self.driver.close()

    def find_price(self):

        try:
            text = self.driver.find_element_by_id("priceblock_ourprice").text
            if text is not None:
                selector = "priceblock_ourprice"
            else:
                raise NoSuchElementException

        except NoSuchElementException:
            try:
                text = self.driver.find_element_by_id("priceblock_dealprice").text
                if text is not None:
                    selector = "priceblock_dealprice"
                else:
                    raise NoSuchElementException

            except NoSuchElementException:
                try:
                    text = self.driver.find_element_by_id("price_inside_buybox").text
                    if text is not None:
                        selector = "price_inside_buybox"
                    else:
                        raise NoSuchElementException

                except NoSuchElementException:

                    try:
                        text = self.driver.find_element(By.CSS_SELECTOR, "#unqualifiedBuyBox>div>div>span").text
                        if text is not None:
                            selector = "unqualifiedBuyBox"
                        else:
                            raise NoSuchElementException

                    except NoSuchElementException:
                        text = self.driver.find_element_by_id("cerberus-data-metrics").get_attribute("data-asin-price")
                        if text is not None:
                            selector = "cerberus-data-metrics"
                        else:
                            raise NoSuchElementException

        except StaleElementReferenceException:
            try:
                text = self.driver.find_element_by_id("priceblock_dealprice").text
                if text is not None:
                    selector = "priceblock_dealprice"
                else:
                    raise NoSuchElementException

            except NoSuchElementException:
                try:
                    text = self.driver.find_element_by_id("price_inside_buybox").text
                    if text is not None:
                        selector = "price_inside_buybox"
                    else:
                        raise NoSuchElementException

                except NoSuchElementException:
                    try:
                        text = self.driver.find_element(By.CSS_SELECTOR, "#unqualifiedBuyBox>div>div>span").text
                        if text is not None:
                            selector = "unqualifiedBuyBox"
                        else:
                            raise NoSuchElementException

                    except NoSuchElementException:
                        text = self.driver.find_element_by_id("cerberus-data-metrics").get_attribute("data-asin-price")
                        if text is not None:
                            selector = "cerberus-data-metrics"
                        else:
                            raise NoSuchElementException

        print("========================================================")
        print("========================================================")
        print("PRICE VALUE IN FRAMEWORK : %r" % text)
        print("SELECTOR CHOICE : %r" % selector)
        print("========================================================")
        print("========================================================")

        return text, selector

    def cerberus_click(self, id):

        timeout = 3
        try:
            WebDriverWait(self.driver, timeout).until(element_to_be_clickable((By.ID, str(id))))

        except TimeoutException:
            pass

        finally:
            self.driver.find_element_by_id(str(id)).click()

    def normal_click(self, option_id, selector_id):

        timeout = 3
        try:
            WebDriverWait(self.driver, timeout).until(element_to_be_clickable((By.ID, str(selector_id))))

        except TimeoutException:
            pass

        finally:
            self.driver.find_element_by_id(str(option_id)).click()

    def css_click(self):

        timeout = 3
        try:
            WebDriverWait(self.driver, timeout).until(element_to_be_clickable((By.CSS_SELECTOR, "#outOfStock>div>div>span")))

        except TimeoutException:
            pass

        finally:
            self.driver.find_element(By.CSS_SELECTOR, "#outOfStock>div>div>span").click()

    def cerberus_wait(self, text, selector_id):

        timeout = 3
        try:
            WebDriverWait(self.driver, timeout).until(Text_To_Change((By.ID, "cerberus-data-metrics"), text))

        except TimeoutException:
            pass

        except StaleElementReferenceException:
            try:
                WebDriverWait(self.driver, timeout).until(Text_To_Change((By.ID, str(selector_id)), text))
                # time.sleep(2)

            except TimeoutException:
                pass

            except StaleElementReferenceException:
                time.sleep(1)

        finally:
            stock = self.get_stock()

        return stock

    def normal_wait(self, text, selector_id):

        timeout = 3
        try:
            WebDriverWait(self.driver, timeout).until(Text_To_Change((By.ID, str(selector_id)), text))

        except TimeoutException:
            pass

        except StaleElementReferenceException:
            try:
                WebDriverWait(self.driver, timeout).until(Text_To_Change((By.ID, "cerberus-data-metrics"), text))
                # time.sleep(2)

            except TimeoutException:
                pass

            except StaleElementReferenceException:
                time.sleep(1)

        finally:
            stock = self.get_stock()

        return stock

    def css_wait(self, text):

        timeout = 3
        try:
            WebDriverWait(self.driver, timeout).until(Text_To_Change((By.CSS_SELECTOR, "#unqualifiedBuyBox>div>div>span::text"), text))

        except TimeoutException:
            pass

        except StaleElementReferenceException:
            try:
                WebDriverWait(self.driver, timeout).until(Text_To_Change((By.ID, "cerberus-data-metrics"), text))
                # time.sleep(2)

            except TimeoutException:
                pass

            except StaleElementReferenceException:
                time.sleep(1)

        finally:
            stock = self.get_stock()

        return stock

    def naive_wait(self, timeout):

        time.sleep(timeout)

        try:
            out_of_stock = self.driver.find_element(By.CSS_SELECTOR, "#outOfStock>div>div>span").text
            stock = out_of_stock

            if stock is not None:
                stock = False
            else:
                stock = True

        except NoSuchElementException:
            stock = True

        return stock

    def buy_button_wait(self, timeout):

        try:
            WebDriverWait(self.driver, timeout).until(element_to_be_clickable((By.ID, "buy-now-button")))

        except TimeoutException:
            pass

        finally:
            stock = self.get_stock()

        return stock

    def asin_wait(self, timeout, asin):

        try:
            WebDriverWait(self.driver, timeout).until(Text_To_Change((By.CSS_SELECTOR, "#cerberus-data-metrics::attr(asin)"), asin))

        except TimeoutException:
            pass

        finally:
            stock = self.get_stock()

        return stock

    def click_option(self, id, selector):

        if "cerberus" not in selector\
                and "unqualified" not in selector:

            self.normal_click(id, selector)
            text, selector = self.find_price()

            if "cerberus" not in selector\
                    and "unqualifiedBuyBox" not in selector:

                # stock = self.naive_wait(3)
                stock = self.normal_wait(text, selector)
                # self.buy_button_wait(3)
                # stock = self.get_stock()

            elif "cerberus" in selector:

                # stock = self.naive_wait(3)
                stock = self.cerberus_wait(text, selector)
                # self.buy_button_wait(3)
                # stock = self.get_stock()

            else:

                stock = self.css_wait(text)
                # self.buy_button_wait(3)
                # stock = self.get_stock()

        elif "cerberus" in selector:

            self.cerberus_click(id)
            text, selector = self.find_price()

            if "cerberus" not in selector\
                    and "unqualifiedBuyBox" not in selector:

                # stock = self.naive_wait(3)
                stock = self.normal_wait(text, selector)
                # self.buy_button_wait(3)
                # stock = self.get_stock()

            elif "cerberus" in selector:

                # stock = self.naive_wait(3)
                stock = self.cerberus_wait(text, selector)
                # self.buy_button_wait(3)
                # stock = self.get_stock()

            else:

                stock = self.css_wait(text)
                # self.buy_button_wait(3)
                # stock = self.get_stock()

        else:

            self.normal_click(id, selector)
            text, selector = self.find_price()

            if "cerberus" not in selector\
                    and "unqualifiedBuyBox" not in selector:

                # stock = self.naive_wait(3)
                stock = self.normal_wait(text, selector)
                # self.buy_button_wait(3)
                # stock = self.get_stock()

            elif "cerberus" in selector:

                # stock = self.naive_wait(3)
                stock = self.cerberus_wait(text, selector)
                # self.buy_button_wait(3)
                # stock = self.get_stock()

            else:

                stock = self.css_wait(text)
                # self.buy_button_wait(3)
                # stock = self.get_stock()

        return text, stock

    def create_scrapy_response(self):

        body = str.encode(self.driver.page_source)
        return HtmlResponse(self.driver.current_url, body=body, encoding="utf-8")

    def get_stock(self):

        try:
            out_of_stock = self.driver.find_element(By.CSS_SELECTOR, "#outOfStock>div>div>span").text
            stock = out_of_stock

            if stock is not None:
                stock = False
            else:
                stock = True

        except NoSuchElementException:
            stock = True

        return stock


class OptionsClicker:

    def __init__(self, response):

        self.response = response

    def get_single_button_items(self, option_ids):

        selenium = SeleniumFramework(self.response, headless=True)

        for x in option_ids:

            colour_class = selenium.get_class(x)
            colour_option = selenium.get_title(x)

            if "Unavailable" not in colour_class \
                    and "Select" not in colour_class:

                text, selector = selenium.find_price()

                text, stock = selenium.click_option(x, selector)

                if stock is True:

                    text, selector = selenium.find_price()

                    deal_name = self.response.css("#productTitle::text").extract_first()

                    option = str(colour_option)

                    response = selenium.create_scrapy_response()

                    page = AmazonPage(response)

                    loader = page.get_item_2(deal_name, option, text)

                    yield loader.load_item()

            elif "Select" in colour_class:

                stock = selenium.get_stock()

                if stock is True:

                    text, selector = selenium.find_price()

                    deal_name = self.response.css("#productTitle::text").extract_first()

                    option = str(colour_option)

                    response = selenium.create_scrapy_response()

                    page = AmazonPage(response)

                    loader = page.get_item_2(deal_name, option, text)

                    yield loader.load_item()

        return selenium.close_driver()

    def get_single_dd_items(self, option_ids):

        selenium = SeleniumFramework(self.response, headless=True)

        for x in option_ids:

            option_class = selenium.get_class(x)
            option = selenium.get_text(x)

            if "Unavailable" not in option_class \
                    and "Select" not in option_class:

                text, selector = selenium.find_price()

                text, stock = selenium.click_option(x, selector)

                if stock is True:

                    text, selector = selenium.find_price()

                    option = str(option)

                    deal_name = self.response.css("#productTitle::text").extract_first()

                    response = selenium.create_scrapy_response()

                    page = AmazonPage(response)

                    loader = page.get_item_2(deal_name, option, text)

                    yield loader.load_item()

            elif "Select" in option_class:

                stock = selenium.get_stock()

                if stock is True:

                    text, selector = selenium.find_price()

                    option = str(option)

                    deal_name = self.response.css("#productTitle::text").extract_first()

                    response = selenium.create_scrapy_response()

                    page = AmazonPage(response)

                    loader = page.get_item_2(deal_name, option, text)

                    yield loader.load_item()

        return selenium.close_driver()

    def get_single_button_single_dd_items(self, option_ids, dd_option_ids):

        selenium = SeleniumFramework(self.response, headless=True)

        for x in dd_option_ids:

            if "_-1" not in x:

                text, selector = selenium.find_price()

                selenium.click_option(x, selector)

            for y in option_ids:

                colour_class = selenium.get_class(y)

                if "Unavailable" not in colour_class \
                        and "Select" not in colour_class:

                    text, selector = selenium.find_price()

                    text, stock = selenium.click_option(y, selector)

                    if stock is True:

                        text, selector = selenium.find_price()

                        size_option = selenium.get_text(x)
                        colour_option = selenium.get_title(y)

                        option = str(colour_option) + str(size_option)

                        deal_name = self.response.css("#productTitle::text").extract_first()

                        response = selenium.create_scrapy_response()

                        page = AmazonPage(response)

                        loader = page.get_item_2(deal_name, option, text)

                        yield loader.load_item()

                elif "Select" in colour_class:

                    stock = selenium.get_stock()

                    if stock is True:

                        text, selector = selenium.find_price()

                        size_option = selenium.get_text(x)
                        colour_option = selenium.get_title(y)

                        option = str(colour_option) + str(size_option)

                        deal_name = self.response.css("#productTitle::text").extract_first()

                        response = selenium.create_scrapy_response()

                        page = AmazonPage(response)

                        loader = page.get_item_2(deal_name, option, text)

                        yield loader.load_item()

        return selenium.close_driver()

    def get_double_button_items(self, option_ids_1, option_ids_2):

        selenium = SeleniumFramework(self.response, headless=True)

        for x in option_ids_1:

            text, selector = selenium.find_price()

            selenium.click_option(x, selector)

            for y in option_ids_2:

                colour_class = selenium.get_class(y)

                if "Unavailable" not in colour_class \
                        and "Select" not in colour_class:

                    text, selector = selenium.find_price()

                    text, stock = selenium.click_option(y, selector)

                    if stock is True:

                        text, selector = selenium.find_price()

                        size_option = selenium.get_title(x)
                        colour_option = selenium.get_title(y)

                        option = str(colour_option) + str(size_option)

                        deal_name = self.response.css("#productTitle::text").extract_first()

                        response = selenium.create_scrapy_response()

                        page = AmazonPage(response)

                        loader = page.get_item_2(deal_name, option, text)

                        yield loader.load_item()

                elif "Select" in colour_class:

                    stock = selenium.get_stock()

                    if stock is True:

                        text, selector = selenium.find_price()

                        size_option = selenium.get_title(x)
                        colour_option = selenium.get_title(y)

                        option = str(colour_option) + str(size_option)

                        deal_name = self.response.css("#productTitle::text").extract_first()

                        response = selenium.create_scrapy_response()

                        page = AmazonPage(response)

                        loader = page.get_item_2(deal_name, option, text)

                        yield loader.load_item()

        return selenium.close_driver()

    def get_double_dd_items(self, dd_option_ids_1, dd_option_ids_2):

        selenium = SeleniumFramework(self.response, headless=True)

        for x in dd_option_ids_1:

            if "_-1" not in x:

                text, selector = selenium.find_price()

                selenium.click_option(x, selector)

            for y in dd_option_ids_2:

                colour_class = selenium.get_class(y)

                if "Unavailable" not in colour_class \
                        and "Select" not in colour_class:

                    text, selector = selenium.find_price()

                    text, stock = selenium.click_option(y, selector)

                    if stock is True:

                        text, selector = selenium.find_price()

                        size_option = selenium.get_text(x)
                        colour_option = selenium.get_text(y)

                        option = str(colour_option) + str(size_option)

                        deal_name = self.response.css("#productTitle::text").extract_first()

                        response = selenium.create_scrapy_response()

                        page = AmazonPage(response)

                        loader = page.get_item_2(deal_name, option, text)

                        yield loader.load_item()

                elif "Select" in colour_class:

                    stock = selenium.get_stock()

                    if stock is True:

                        text, selector = selenium.find_price()

                        size_option = selenium.get_text(x)
                        colour_option = selenium.get_text(y)

                        option = str(colour_option) + str(size_option)

                        deal_name = self.response.css("#productTitle::text").extract_first()

                        response = selenium.create_scrapy_response()

                        page = AmazonPage(response)

                        loader = page.get_item_2(deal_name, option, text)

                        yield loader.load_item()

        return selenium.close_driver()

    def get_triple_button_items(self, option_ids_1, option_ids_2, option_ids_3):

        selenium = SeleniumFramework(self.response, headless=True)

        for x in option_ids_1:

            text, selector = selenium.find_price()

            selenium.click_option(x, selector)

            for y in option_ids_2:

                colour_class = selenium.get_class(y)

                if "Unavailable" not in colour_class:

                    text, selector = selenium.find_price()

                    selenium.click_option(y, selector)

                    for z in option_ids_3:

                        colour_class = selenium.get_class(z)

                        if "Unavailable" not in colour_class:

                            text, selector = selenium.find_price()

                            text, stock = selenium.click_option(z, selector)

                            if stock is True:

                                text, selector = selenium.find_price()

                                style_option = selenium.get_title(x)
                                colour_option = selenium.get_title(y)
                                size_option = selenium.get_title(z)

                                deal_name = self.response.css("#productTitle::text").extract_first()

                                option = str(colour_option) + str(style_option) + str(size_option)

                                response = selenium.create_scrapy_response()

                                page = AmazonPage(response)

                                loader = page.get_item_2(deal_name, option, text)

                                yield loader.load_item()

        return selenium.close_driver()

    def get_single_dd_double_button_items(self, dd_options, button_ops_1, button_ops_2):

        selenium = SeleniumFramework(self.response, headless=True)

        for x in dd_options:

            if "-1" not in x:

                text, selector = selenium.find_price()

                selenium.click_option(x, selector)

            for y in button_ops_1:

                colour_class = selenium.get_class(y)

                if "Unavailable" not in colour_class:

                    text, selector = selenium.find_price()

                    selenium.click_option(y, selector)

                    for z in button_ops_2:

                        colour_class = selenium.get_class(z)

                        if "Unavailable" not in colour_class:

                            text, selector = selenium.find_price()

                            text, stock = selenium.click_option(z, selector)

                            if stock is True:

                                text, selector = selenium.find_price()

                                style_option = selenium.get_text(x)
                                colour_option = selenium.get_title(y)
                                size_option = selenium.get_title(z)

                                deal_name = self.response.css("#productTitle::text").extract_first()

                                option = str(colour_option) + str(style_option) + str(size_option)

                                response = selenium.create_scrapy_response()

                                page = AmazonPage(response)

                                loader = page.get_item_2(deal_name, option, text)

                                yield loader.load_item()

        return selenium.close_driver()

    def get_triple_dd_items(self, options_1, options_2, options_3):

        selenium = SeleniumFramework(self.response, headless=True)

        for x in options_1:

            if "_-1" not in x:

                text, selector = selenium.find_price()

                selenium.click_option(x, selector)

            for y in options_2:

                colour_class = selenium.get_class(y)

                if "Unavailable" not in colour_class \
                        and "Select" not in colour_class:

                    text, selector = selenium.find_price()

                    text, stock = selenium.click_option(y, selector)

                    for z in options_3:

                        option_3_class = selenium.get_class(z)

                        if "Unavailable" not in option_3_class \
                                and "Select" not in option_3_class:

                            if stock is True:

                                text, selector = selenium.find_price()

                                size_option = selenium.get_text(x)
                                colour_option = selenium.get_text(y)
                                option_3 = selenium.get_text(z)

                                option = str(colour_option) + str(size_option) + str(option_3)

                                deal_name = self.response.css("#productTitle::text").extract_first()

                                response = selenium.create_scrapy_response()

                                page = AmazonPage(response)

                                loader = page.get_item_2(deal_name, option, text)

                                yield loader.load_item()

                        elif "Select" in option_3_class:

                            stock = selenium.get_stock()

                            if stock is True:

                                text, selector = selenium.find_price()

                                size_option = selenium.get_text(x)
                                colour_option = selenium.get_text(y)
                                option_3 = selenium.get_text(z)

                                option = str(colour_option) + str(size_option) + str(option_3)

                                deal_name = self.response.css("#productTitle::text").extract_first()

                                response = selenium.create_scrapy_response()

                                page = AmazonPage(response)

                                loader = page.get_item_2(deal_name, option, text)

                                yield loader.load_item()

                elif "Select" in colour_class:

                    text, selector = selenium.find_price()

                    text, stock = selenium.click_option(y, selector)

                    for z in options_3:

                        option_3_class = selenium.get_class(z)

                        if "Unavailable" not in option_3_class \
                                and "Select" not in option_3_class:

                            if stock is True:

                                text, selector = selenium.find_price()

                                size_option = selenium.get_text(x)
                                colour_option = selenium.get_text(y)
                                option_3 = selenium.get_text(z)

                                option = str(colour_option) + str(size_option) + str(option_3)

                                deal_name = self.response.css("#productTitle::text").extract_first()

                                response = selenium.create_scrapy_response()

                                page = AmazonPage(response)

                                loader = page.get_item_2(deal_name, option, text)

                                yield loader.load_item()

                        elif "Select" in option_3_class:

                            stock = selenium.get_stock()

                            if stock is True:

                                text, selector = selenium.find_price()

                                size_option = selenium.get_text(x)
                                colour_option = selenium.get_text(y)
                                option_3 = selenium.get_text(z)

                                option = str(colour_option) + str(size_option) + str(option_3)

                                deal_name = self.response.css("#productTitle::text").extract_first()

                                response = selenium.create_scrapy_response()

                                page = AmazonPage(response)

                                loader = page.get_item_2(deal_name, option, text)

                                yield loader.load_item()

        return selenium.close_driver()

    def get_double_dd_single_button_items(self, dd_options_1, dd_options_2, button_options):

        selenium = SeleniumFramework(self.response, headless=True)

        for x in dd_options_1:

            if "_-1" not in x:

                text, selector = selenium.find_price()

                selenium.click_option(x, selector)

            for y in dd_options_2:

                if "_-1" not in y:

                    text, selector = selenium.find_price()

                    selenium.click_option(y, selector)

                    for z in button_options:

                        colour_class = selenium.get_class(y)

                        if "Unavailable" not in colour_class:
                            text, selector = selenium.find_price()

                            text, stock = selenium.click_option(y, selector)

                            if stock is True:

                                text, selector = selenium.find_price()

                                style_option = selenium.get_text(x)
                                colour_option = selenium.get_text(y)
                                size_option = selenium.get_title(z)

                                deal_name = self.response.css("#productTitle::text").extract_first()

                                option = str(colour_option) + str(style_option) + str(size_option)

                                response = selenium.create_scrapy_response()

                                page = AmazonPage(response)

                                loader = page.get_item_2(deal_name, option, text)

                                yield loader.load_item()

        return selenium.close_driver()
