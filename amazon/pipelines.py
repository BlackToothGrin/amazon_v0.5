from scrapy.exporters import CsvItemExporter
from datetime import datetime


class CsvPipeline(object):

    """Pipeline for outputting scraped items to CSV"""

    def __init__(self):
        date = str(datetime.now().date())
        self.file = open("test_7_" + date + ".csv", 'wb')
        self.exporter = CsvItemExporter(self.file, 'unicode')
        self.exporter.start_exporting()

    def close_spider(self, spider):
        self.exporter.finish_exporting()
        self.file.close()

    def process_item(self, item, spider):
        self.exporter.export_item(item)
        return item