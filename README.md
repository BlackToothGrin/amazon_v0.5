## **SELENIUM_FRAMEWORK.py**

#### **CLASS** _Text_To_Change_:

**A custom expected condition for Selenium from stack-overflow: waits for the value of a given element to change**

#### methods:   
 > \_\_init__
 >>Instanciates the **Text_To_Change** class
 >>>Takes two arguments: _locator, text_

  >\_\_call__
  >>Implements function call on **Text_To_Change** object
  >>>Takes one argument: _dirver_
  >>>>**Returns** a boolean _True/False_ value

#### **CLASS** _SELENIUM_FRAMEWORK_:

**A class containing all basic Selenium functionality such as fetching selector attributes, clicking on elements and waiting for conditions to be satisfied**

#### methods:
>\_\_init__
>>Instanciates the **Selenium_Framework** class, configuring and launching a selenium webdriver instance and navigating to the response url
>>>Takes one argument and one key-word argument:
>>>_response_ object from scrapy and _headless=False_ by default

>**get_class**
>>Fetches the _class_ attribute for a given selector id
>>>Takes one argument: _Id_
>>>>**Returns** a _class_ attribute

>**get_title**
>>Fetches the _title_ attribute for a given selector id
>>>Takes one argument: _Id_
>>>>**Returns** a _title_ attribute

>**get_text**
>>Fetches the _text_ attribute for a given selector id
>>>Takes one argument: _Id_
>>>>**Returns** a _text_ attribute

>**get_text_by_css**
>>Fetches the _text_ attribute from a given css selector
>>>Takes one argument: _css selector_
>>>>**Returns** a _text_ attribute

>**get_attribute_by_id**
>>Fetches a given attribute given a selector id
>>>Takes two arguments: _Id, Attribute_
>>>>**Returns** the given attribute

>**close_driver**
>>Closes the selenium driver
>>>Takes no arguments
>>>>**Returns** _driver.close_driver()_

>**find_price**
>>A mess of _try/except_ statements usefull for finding the updated _price_ selector after clicking on an option
>>>Takes no arguments
>>>>**Returns** _text, selector_ (text is the price value, selector is the selector associated with that value)

>**cerberus_click**
>>Waits for element at given id to be clickable then clicks on it.
>>Is formatted specifically for when the price selector has been located with the cerberus selector
>>>Takes one argument: _Id_

>**normal_click**
>>A more generalised click function: waits for element at the given _selector_id_ to be clickable,
>>then clicks on the given _option_id_
>>>Takes two arguments: _option_id, selector_id_

>**css_click**
>>A click function specific to the _"#outOfStock>div>div>span"_ selector
>>>Takes no arguments

>**cerberus_wait**
>>After an option has been clicked, if the price selector was previously _"cerberus"_ style, this function will wait for the page to reload with a new price. Function will also detect whether the chosen option is in stock and return a boolean stock value.
>>>Takes two arguments: _text, selector_id_
>>>>**Returns** _stock_ - a boolean indicating whether the chosen option is in stock

>**normal_wait**
>>A more generalised wait function that has more robust logic
>>>Takes two arguments: _text, selector_id_
>>>>**Returns** _stock_ - a boolean indicating whether the chosen option is in stock

>**css_wait**
>>An older wait function specific to the _"#unqualifiedBuyBox>div>div>span"_ selector
>>>Takes one argument: _text_
>>>>**Returns** _stock_

>**naive_wait**
>>A Naive wait function for testing/debug purposes: simply waits for n secounds then checks if the option is in stock
>>>Takes one argument: _timeout_ - time to sleep
>>>>**Returns** _stock_

>**buy_button_wait**
>>Waits for the buy button to reload on the page before checking if the chosen option is in stock
>>>Takes one argument: _timeout_
>>>>**Returns** _stock_

>**asin_wait**
>>Waits for the ASIN number to change before checking if the chosen option is in stock
>>>Takes two arguments: _timeout, asin_
>>>>**Returns** _stock_

>**click_option**
>>A mess of _try/except_ statements designed orchestrate the clicking of an option given the current price _selector_ and _option id_
>>>Takes two arguments: _Id, selector_
>>>>**Returns** _text, stock_ (text is the new price value)

>**create_scrapy_response**
>>Encodes the current Selenium page into a format readable by scrapy, then packages the page as a scrapy compatible response object
>>>Takes no arguments
>>>>**Returns** a scrapy _response_ object

>**get_stock**
>>Checks if an item is in stock
>>>Takes no arguments
>>>>**Returns** _stock_ boolean value

#### CLASS OPTIONS_CLICKER:

**Contains all selenium orchestration logic for clicking through options on an item page**

#### methods:    
>**\_\_init__**
>>Instanciates the **Options_Clicker** class
>>>Takes one argument: a scrapy response object

>**get_single_button_items** (generator function)
>>Clicks through all options for a page with a single set of button-style options
>>>Takes one argument: a list of options selectors
>>>>**Yields** _loader.load_item()_   
>>>>**Returns** _selenium.close_driver()_

>**get_double_button_items** (generator function)
>>Clicks through all options for a page with two sets of button-style options
>>>Takes two arguments: _option_1_ids, option_2_ids_
>>>>**Yields** _loader.load_item()_
>>>>**Returns** _selenium.close_driver()_

>**get_triple_button_items** (generator function)
>>Clicks through all options for a page with three sets of button-style options
>>>Takes three arguments: _option_1_ids, option_2_ids, option_3_ids_
>>>>**Yields** _loader.load_item()_
>>>>**Returns** _selenium.close_driver()_

>**get_single_dd_items** (generator function)
>>Clicks through all options for a page with a single drop-down box
>>>Takes one argument: _option_ids_
>>>>**Yields** _loader.load_item()_
>>>>**Returns** _selenium.close_driver()_

>**get_double_dd_items** (generator function)
>>Clicks through all options for a page with two drop-down boxes
>>>Takes two arguments: _option_1_ids, option_2_ids_
>>>>**Yields** _loader.load_item()_
>>>>**Returns** _selenium.close_driver()_

>**get_triple_dd_items** (generator function)
>>Clicks through all options for a page with three drop-down boxes
>>>Takes three arguments: _option_1_ids, option_2_ids, option_3_ids_
>>>>**Yields** _loader.load_item()_
>>>>**Returns** _selenium.close_driver()_

>**get_single_button_single_dd_items** (generator function)
>>Clicks through all options for a page with a single set of button-style options and a single drop-down box
>>>Takes two arguments: _option_ids, dd_option_ids_
>>>>**Yields** _loader.load_item()_
>>>>**Returns** _selenium.close_driver()_

>**get_single_dd_double_button_items** (generator function)
>>Clicks through all options for a page with a single drop-down and two sets of button-style options
>>>Takes three arguments: _dd_option_ids, option_1_ids, option_2_ids_
>>>>**Yields** _loader.load_item()_
>>>>**Returns** _selenium.close_driver()_

>**get_double_dd_single_button_items** (generator function)
>>Clicks through all options for a page with two drop-downs and one set of button-style options
>>>Takes three arguments: _dd_option_1_ids, dd_option_2_ids, option_ids_
>>>>**Yields** _loader.load_item()_
>>>>**Returns** _selenium.close_driver()_


## **SCRAPY_FRAMEWORK.py**

#### CLASS AMAZON_PAGE:

**Contains all Scrapy logic for navigating through amazon, extracting links, finding options selectors and extracting item data**

#### methods:
>**\_\_init__**
>>Instanciates the **Amazon_Page** class
>>>Takes one argument: _response_ object from scrapy

>**get_categories**
>>Finds sub-category urls on a given page
>>>Takes no arguments
>>>>**Returns** list of urls

>**generate_request**
>>Generates a scrapy request to a given url with callback to a pre-defined function
>>>Takes two arguments: _url and callback_
>>>>**Returns** a request object

>**get_item_requests**
>>Generates scrapy requests to item pages
>>>Takes one argument: _callback_
>>>>**Returns** a request object

>**paginate**
>>Clicks the "Next" button - allows for paginating through item pages
>>>Takes one argument: _callback_
>>>>**Returns** a request object

>**get_ids**  (generator function)
>>Fetches all id attributes given a css selector
>>>Takes one argument: _selector_
>>>>**Yields** id attributes

>**find_styles**
>>Identifies new page styles based on 14 different selector styles
>>>Takes one argument: a _list of selectors_ to check against (can be none)
>>>>**Returns** a list of selectors (unique from the input list)

>**get_item** (Depreciated)
>>Extracts data from an item page
>>>Arguments: _deal_name, option, text, best_sellers_rank, asin, star_rating, star_rating_5,
                 star_rating_4, star_rating_3, star_rating_2, star_rating_1, prime_eligible, date_available,
                 model_number_
>>>>**Returns** an _item_loader_ object (fully loaded, ready for _load_items()_)

>**get_item_2** (Replaces _get_item_)
>>Extracts data from an item page
>>>Takes three arguments: _deal_name, option, text_ (text = price value)
>>>>**Returns** an _item_loader_ object (fully loaded, ready for _load_items()_)

>**find_options**
>>Finds all types of options selectors on a page, returning a dictionary conatining the number of 
dropdown-style options, number of button-style options and id selectors for all options.
>>>Takes no arguments
>>>>**Returns** _Options_: _{"button_number": int, "dropdown_number": int, "button_dict": {"button_option_1_ids": [ids], "button_option_2_ids": [ids], "button_option_3_ids", [ids], "button_option_4_ids": [ids]}, "dropdown_dict": {"dd_option_1_ids": [ids], "dd_option_2_ids": [ids], "dd_option_3_ids": [ids], "dd_option_4_ids": [ids]}}_

###### Scraping project for Amazon UK using Scrapy and Selenium

###### Created by Ross Considine and Brett Taylor 2019